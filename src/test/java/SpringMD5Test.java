import com.woniu.springmybatis.entity.Account;
import com.woniu.springmybatis.service.AccountService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring_config.xml")
public class SpringMD5Test {
    @Resource
  private AccountService accountService;
    @Test
    public void insert(){
        Account account =new Account();
        // account表主键已设置自增，不需要手动设置id
        // account.setId(2);
        account.setUsername("admin");
        account.setPassword("admin");
        this.accountService.saveUserInfor(account);
    }
}
