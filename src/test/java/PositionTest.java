import com.woniu.springmybatis.entity.Position;
import com.woniu.springmybatis.mapper.PositionMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring_config.xml")
public class PositionTest {
	@Resource
	private PositionMapper positionMapper;

	/**
	 * 得到职位列表
	 */
	@Test
	public void test(){
		List<Position> lists = this.positionMapper.selectAll();
		for (int i = 0; i < lists.size(); i++) {
			System.out.println(lists.get(i));
		}
	}
}
