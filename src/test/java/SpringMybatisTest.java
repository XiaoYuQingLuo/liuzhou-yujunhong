import com.woniu.springmybatis.entity.Account;
import com.woniu.springmybatis.mapper.AccountMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring_config.xml")
public class SpringMybatisTest {
    @Resource
    private AccountMapper accountMapper;
    @Test
    public void findUserById(){
        Account account = this.accountMapper.selectByPrimaryKey(1);
//        Account userById = this.accountMapper.findUserById(2);

        System.out.println(account.getUsername());
    }
}
