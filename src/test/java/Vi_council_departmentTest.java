import com.woniu.springmybatis.entity.Vi_council_department;
import com.woniu.springmybatis.mapper.Vi_council_departmentMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring_config.xml")
public class Vi_council_departmentTest {
	@Resource
	private Vi_council_departmentMapper vi_council_departmentMapper;

	@Test
	public void test() {
		List<Vi_council_department> lists = this.vi_council_departmentMapper.selectAll();
		for (int i = 0; i < lists.size(); i++) {
			System.out.println(lists.get(i).getD_name()+"=="+lists.get(i).getCouncil_name());
		}
	}
}
