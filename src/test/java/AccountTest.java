import com.woniu.springmybatis.entity.Account;
import com.woniu.springmybatis.mapper.AccountMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring_config.xml")
public class AccountTest {
	@Resource
	private AccountMapper accountMapper;
	// 验证重复登录名
	@Test
	public void test1() {
		String str = "admin";
		Example example = new Example(Account.class);
		example.createCriteria().andEqualTo("username",str);
		int i = this.accountMapper.selectCountByExample(example);
		System.out.println(i);
	}
}
