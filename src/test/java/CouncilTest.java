import com.woniu.springmybatis.entity.Council;
import com.woniu.springmybatis.mapper.CouncilMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring_config.xml")
public class CouncilTest {
	@Resource
	private CouncilMapper councilMapper;

	@Test
	public void test() {
		List<Council> councils = this.councilMapper.selectAll();
		for (int i = 0; i < councils.size(); i++) {
			System.out.println(councils.get(i));
		}
	}
}
