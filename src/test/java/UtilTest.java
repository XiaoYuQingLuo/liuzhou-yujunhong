import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.DigestUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring_config.xml")
public class UtilTest {

	@Test
	public void test() {
		/*String str = "admin3";
		String s = DigestUtils.md5DigestAsHex(str.getBytes());
		System.out.println(s);*/
		for (int i = 4; i < 12; i++) {
			String str = "admin" + i;
			String s = DigestUtils.md5DigestAsHex(str.getBytes());
			System.out.println(i + "========" + s);
		}
	}

	@Test
	public void test1() {
		String str1 = "[{\"dep_id\":\"1\",\"dep_name\":\"招商部\"},{\"dep_id\":\"2\",\"dep_name\":\"财务部\"}]";
		String str = str1.substring(1,str1.length()-1);
		System.out.println("截取后："+str);

	}
}
