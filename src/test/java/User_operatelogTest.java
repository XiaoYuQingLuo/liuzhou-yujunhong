import com.woniu.springmybatis.entity.User_operatelog;
import com.woniu.springmybatis.service.User_operatelogService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring_config.xml")
public class User_operatelogTest {

    @Resource
    private User_operatelogService user_operatelogService;
    @Test
    public  void insert(){
        User_operatelog user_operatelog = new User_operatelog();
        //获取操作人用户名id
        user_operatelog.setUser_id(2);
        //获取操作方法
        user_operatelog.setModule_name("测试");
        //获取操作内容
        user_operatelog.setOperate_desc("测试");
        //获取ip地址
        user_operatelog.setIp_address("");
        //获取操作时间
        user_operatelog.setTime(new Date());
        user_operatelogService.insertLog(user_operatelog);
    }

}
