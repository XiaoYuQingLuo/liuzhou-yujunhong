import com.woniu.springmybatis.entity.Vi_account_user_depatment_position_council;
import com.woniu.springmybatis.mapper.Vi_account_user_depatment_position_councilMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring_config.xml")
public class Vi_account_user_depatment_position_councilTest {
	@Resource
	private Vi_account_user_depatment_position_councilMapper mapper;

	@Test
	public void test() {
		List<Vi_account_user_depatment_position_council> lists = this.mapper.selectAll();
		for (int i = 0; i < lists.size(); i++) {
			System.out.println(lists.get(i));
		}
	}

	@Test
	public void searchUser() {
		String context = "招商部";
		Example example = new Example(Vi_account_user_depatment_position_council.class);
		example.createCriteria().andCondition("d_name=", context);
		List<Vi_account_user_depatment_position_council> lists = this.mapper.selectByExample(example);
		for (int i = 0; i < lists.size(); i++) {
			System.out.println(lists.get(i));
		}

	}

	@Test
	public void findAllThing() {
		Example example = new Example(Vi_account_user_depatment_position_council.class);
		example.createCriteria().andCondition("u_id=", 8);
		List<Vi_account_user_depatment_position_council> lists = this.mapper.selectByExample(example);
		for (int i = 0; i < lists.size(); i++) {
			System.out.println(lists.get(i));
		}
	}

	// 复杂查询
	@Test
	public void test1(){
		Example example = new Example(Vi_account_user_depatment_position_council.class);
		Example.Criteria c1 = example.createCriteria();
		Example.Criteria c2 = example.createCriteria();
		String searchText = "测试部门";
		c1.orEqualTo("name",searchText).orEqualTo("tel",searchText).orEqualTo("email",searchText).
				orEqualTo("sex",searchText).orEqualTo("d_name",searchText).orEqualTo("p_name",searchText).
				orEqualTo("council_name",searchText);
		c2.andEqualTo("flag",0);
		example.and(c2);
		List<Vi_account_user_depatment_position_council> lists = this.mapper.selectByExample(example);
		for (int i = 0; i < lists.size(); i++) {
			System.out.println(lists.get(i));
		}
	}
}
