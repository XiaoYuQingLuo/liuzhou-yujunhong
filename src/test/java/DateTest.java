import org.junit.Test;

import java.text.ParseException;

public class DateTest {
    @Test
    public void Date(){
        java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            java.util.Date beginDate= format.parse("2008-12-24 12:50:00");
            java.util.Date endDate= format.parse("2008-12-25 14:30:00");
            long day=(endDate.getTime()-beginDate.getTime())/(24*60*60*1000);
            System.out.println("相隔的天数="+day);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}
