import com.woniu.springmybatis.entity.Notification;
import com.woniu.springmybatis.entity.Vi_Notif_User;
import com.woniu.springmybatis.mapper.NotificationMapper;
import com.woniu.springmybatis.mapper.Vi_Notif_UserMapper;
import com.woniu.springmybatis.service.NotificationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import javax.annotation.Resource;
import java.util.List;

/**
 * 个人信息测试类
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring_config.xml")
public class NotificationTest {
    @Resource
    private NotificationMapper notificationMapper;
    @Resource
    private NotificationService notificationService;
    @Resource
    private Vi_Notif_UserMapper vi_notif_userMapper;

@Test
    public void findById(){

       List<Vi_Notif_User> vi_notif_users = notificationService.idFindNotif(1);
        System.out.println(vi_notif_users.get(0).getSend_time());


    }
    @Test
    public  void updateByid(){
        int i = notificationService.changStateID(2);
        System.out.println(i);
    }
    @Test
    public void insert(){
        int i = notificationService.insertNotifi(1,1,
                "test2","test2");
        System.out.println(i);
    }
}
