import com.woniu.springmybatis.entity.Department;
import com.woniu.springmybatis.mapper.DepartmentMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring_config.xml")
public class DepartmentTest {
	@Resource
	private DepartmentMapper departmentMapper;

	@Test
	public void test() {
		List<Department> deps = this.departmentMapper.selectAll();
		for (int i = 0; i < deps.size(); i++) {
			System.out.println(deps.get(i));
		}
	}

	@Test
	public void test1() {
		Department department = this.departmentMapper.selectByPrimaryKey(1);
		System.out.println(department);
	}

	@Test
	public void test2() {
		Department dep = new Department();
		dep.setId(1);
		dep.setD_name("测试");
		dep.setD_text("");
		int i = this.departmentMapper.updateByPrimaryKeySelective(dep);
		System.out.println("受影响的行：" + i);
	}

	@Test
	public void test3() {
		Department dep = new Department();
		dep.setId(5);
		int i = this.departmentMapper.deleteByPrimaryKey(dep);
		System.out.println("受影响的行：" + i);
	}
}
