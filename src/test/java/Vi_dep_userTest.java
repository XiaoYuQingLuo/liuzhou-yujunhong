import com.woniu.springmybatis.mapper.Vi_dep_userMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.woniu.springmybatis.entity.Vi_dep_user;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring_config.xml")

public class Vi_dep_userTest {
	@Resource
	Vi_dep_userMapper vi_dep_userMapper;
	@Test
	public void test(){
		Example example = new Example(Vi_dep_user.class);
		example.createCriteria().andCondition("id=",1);
		List<Vi_dep_user> lists = this.vi_dep_userMapper.selectByExample(example);
		for (int i = 0; i < lists.size(); i++) {
			System.out.println(lists.get(i));
		}
	}
}
