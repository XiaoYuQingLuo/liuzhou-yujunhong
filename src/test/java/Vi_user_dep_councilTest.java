import com.woniu.springmybatis.entity.Vi_user_dep_council;
import com.woniu.springmybatis.mapper.Vi_user_dep_councilMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring_config.xml")
public class Vi_user_dep_councilTest {
	@Resource
	private Vi_user_dep_councilMapper vi_user_dep_councilMapper;

	@Test
	public void test() {
		/*List<Vi_user_dep_council> lists = this.vi_user_dep_councilMapper.selectAll();*/
		List<Vi_user_dep_council> lists = this.vi_user_dep_councilMapper.selectAll();
		for (int i = 0; i < lists.size(); i++) {
			System.out.println(lists.get(i));
		}
	}

	@Test
	public void test1() {
		Example example = new Example(Vi_user_dep_council.class);
		example.createCriteria().andCondition("d_id=",1);
		List<Vi_user_dep_council> vi_user_dep_councils = this.vi_user_dep_councilMapper.selectByExample(example);
		for (int i = 0; i < vi_user_dep_councils.size(); i++) {
			System.out.println(vi_user_dep_councils.get(i));

		}
	}
	@Test
	public void test2(){
		Example example = new Example(Vi_user_dep_council.class);
		example.createCriteria().andCondition("d_id=",1);
		List<Vi_user_dep_council> vi_user_dep_councils = this.vi_user_dep_councilMapper.selectByExample(example);
		System.out.println(vi_user_dep_councils.get(0));
	}
}
