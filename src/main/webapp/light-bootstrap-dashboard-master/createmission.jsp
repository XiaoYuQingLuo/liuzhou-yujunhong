<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/3/9
  Time: 14:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <%--light-bootstrap-dashboard-master/assets/img/favicon.ico--%>
    <link rel="icon" type="image/png" href="../light-bootstrap-dashboard-master/assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <title>Light Bootstrap Dashboard by Creative Tim</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>


    <!-- Bootstrap core CSS     -->
    <link href="../light-bootstrap-dashboard-master/assets/css/bootstrap.min.css" rel="stylesheet"/>

    <!-- Animation library for notifications   -->
    <link href="../light-bootstrap-dashboard-master/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="../light-bootstrap-dashboard-master/assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="../light-bootstrap-dashboard-master/assets/css/demo.css" rel="stylesheet"/>


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css

" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300

' rel='stylesheet' type='text/css'>
    <link href="../light-bootstrap-dashboard-master/assets/css/pe-icon-7-stroke.css" rel="stylesheet"/>
    <%-- 引入操作框格的CSS --%>
    <link href="../light-bootstrap-dashboard-master/assets/css/permission.css" rel="stylesheet">


    <style>
        .uppage {
            height: 35px;
            width: 80px;
            margin: 0px 300px;
            float: left
        }

        .downpage {
            height: 45px;
            width: 80px;
            margin: 10px 650px;
        }

        .downpage img {
            height: 45px;
            text-align: center;
            line-height: 45px;
        }

        .uppage img {
            height: 45px;
            text-align: center;
            line-height: 45px;
        }
    </style>
</head>

<body>
<div class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="card" style="width: 1340px">
                <div class="header">
                    <h4 class="title">新建子任务</h4>
                </div>
                <div class="content">
                    <form action="" name="form1"
                          method="post">
                        <div class="row">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>子任务名</label>
                                        <input type="text" name="mission_name" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>子任务内容</label>
                                    <input type="text" name="mission_text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>开始日期：</label>
                                    <!--指定 date标记-->
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input type='text' name="start_time" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>计划日期：</label>
                                <!--指定 date标记-->
                                <div class='input-group date' id='datetimepicker2'>
                                    <input type='text' name="plan_time" class="form-control"/>
                                </div>
                            </div>
                        </div>

                        <div class="page-header">
                            <!-- 招商引资库 -->
                            <div class="form-horizontal">
                                <div class="control-label col-lg-0">
                                </div>
                                <%--子任务分配部门 --%>
                                <div class="col-lg-2">
                                    <label>配合部门</label>
                                    <select class="form-control" name="dep_id">
                                        <c:forEach items="${requestScope.departments}" var="department">
                                            <option value="${department.id}" >
                                                    ${department.d_name}
                                            </option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>子任务说明</label>
                                        <textarea rows="5" class="form-control" name="pro_text"></textarea>
                                    </div>
                                </div>
                            </div>
                            <input type="button" value="完成" class="btn btn-info btn-fill pull-right" type="submit" onclick="form1.action='../CreateMissionController/missionSave.do';form1.submit();"/>
                            <input type="button" value="新建子任务" class="btn btn-info btn-fill pull-right" type="submit" onclick="form1.action='../CreateMissionController/addMissionSave.do';form1.submit();"/>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container-fluid">
        <nav class="pull-left">
            <ul>
                <li>
                    <a href="#">
                        Home
                    </a>
                </li>
                <li>
                    <a href="#">
                        Company
                    </a>
                </li>
                <li>
                    <a href="#">
                        Portfolio
                    </a>
                </li>
                <li>
                    <a href="#">
                        Blog
                    </a>
                </li>
            </ul>
        </nav>
        <p class="copyright pull-right">
            &copy;
            <script>document.write(new Date().getFullYear())</script>
            <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
        </p>
    </div>
</footer>
</div>

</div>

<script>
    $(function () {
        $('#datetimepicker1').datetimepicker({
            format: 'YYYY-MM-DD',
            locale: moment.locale('zh-cn')
        });
        $('#datetimepicker2').datetimepicker({
            format: 'YYYY-MM-DD hh:mm',
            locale: moment.locale('zh-cn')
        });
    });
    function manySend(href){
        var form = document.form1;
        form.action = href;//传想要跳转的路径
        form.submit();
    }

</script>
</body>

<script src="../light-bootstrap-dashboard-master/assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../light-bootstrap-dashboard-master/assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="../light-bootstrap-dashboard-master/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="../light-bootstrap-dashboard-master/assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE

"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="../light-bootstrap-dashboard-master/assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

</html>

