<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="icon" type="image/png"
          href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <title>Light Bootstrap Dashboard by Creative Tim</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>


    <!-- Bootstrap core CSS     -->
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/bootstrap.min.css"
          rel="stylesheet"/>

    <!-- Animation library for notifications   -->
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/animate.min.css"
          rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/light-bootstrap-dashboard.css?v=1.4.0"
          rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/demo.css"
          rel="stylesheet"/>


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/pe-icon-7-stroke.css"
          rel="stylesheet"/>
    <title>Title</title>
    <style>
        .projectInfor {
            background-color: #FFFFFF;
            border: 1px solid #E3E3E3;
            border-radius: 4px;
            color: #565656;
            padding: 8px 12px;
            height: 40px;
            -webkit-box-shadow: none;
            box-shadow: none;
        }
    </style>
</head>
<body>
<%-- 部门名称、所属局、部门领导 --%>
<%--<c:forEach items="${requestScope.user_dep_council}" var="user_dep_council">--%>
<%-- 部门详细信息，部门下属员工 --%>
<%--<c:forEach items="${requestScope.dep_user}" var="dep_user" >--%>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card" style="width: 1340px">
                    <div class="header">
                        <h4 class="title">部门详情</h4>
                    </div>
                    <div class="content">
                        <form method="POST" action="../DepartmentController/updateDepInfo.do?d_id=${depInfo.d_id}">
                            <%-- 部门名称、所属局、部门领导、部门简介 --%>
                            <%--<c:forEach items="${requestScope.user_dep_council}" var="user_dep_council">--%>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>所属局</label>
                                    </div>
                                    <div class="projectInfor">
                                        ${depInfo.council_name}
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>部门名称</label>
                                    </div>
                                    <div class="projectInfor">
                                        <input name="d_name" value=${depInfo.d_name} >
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>主管领导</label>
                                    </div>
                                    <div class="projectInfor">
                                        ${depInfo.leader}
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>部门简介</label>
                                    </div>
                                    <div class="projectInfor">
                                        <input name="d_text" value=${depInfo.d_text} >

                                    </div>
                                </div>
                            </div>

                            <%--</c:forEach>--%>

                            <%--==============--%>
                            <br>
                            <br>
                            <table class="table table-hover table-striped">
                                <thead>
                                <th>序号</th>
                                <th>姓名</th>
                                <th>电话</th>
                                <th>邮箱</th>
                                <th>性别</th>
                                </thead>
                                <c:forEach items="${requestScope.dep_users}" var="dep_user" varStatus="status">
                                    <tr>
                                            <%-- 序号 --%>
                                        <td>${status.count}</td>
                                            <%-- 员工姓名 --%>
                                        <td>${dep_user.name}</td>
                                            <%-- 电话号码 --%>
                                        <td>${dep_user.tel}</td>
                                            <%-- 电子邮箱 --%>
                                        <td>${dep_user.email}</td>
                                            <%-- 性别 --%>
                                        <td>${dep_user.sex}</td>
                                    </tr>
                                </c:forEach>
                            </table>
                            <a href="updateDepInfo.do?dep.id=${depInfo.d_id}">
                                <button type="submit" class="btn btn-info btn-fill pull-right">修改部门信息</button>
                            </a>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
            <%--</c:forEach>--%>
</body>
<!--   Core JS Files   -->
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/jquery.3.2.1.min.js"
        type="text/javascript"></script>
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/bootstrap.min.js"
        type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>
</html>