<%--
  Created by IntelliJ IDEA.
  User: tbths
  Date: 2018/3/19
  Time: 15:23
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%@ page contentType="application/json" pageEncoding="UTF-8"%>--%>
<html>
<head>
    <title>测试二级联动</title>
    <script src="../light-bootstrap-dashboard-master/assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
</head>
<body>
<div class="form-group">
    <div class="col-xs-12">
        <div class="">
            <label class="control-label col-xs-3 text-right">
                局级单位名称：
            </label>
            <div class="col-xs-8">
                <select name="source" id="source" class="form-control" onchange="jumpPage();">
                    <option value="">请选择</option>
                    <c:forEach items="${councilList}" var="list">
                        <option value="${list.id}">
                            <span style="white-space:pre"></span>
                                ${list.council_name}
                            <span style="white-space:pre"></span>
                        </option>
                    </c:forEach>
                </select>
            </div>
        </div>
    </div>

    <%--第二个下拉框--%>
    <div class="">
        <div class="col-xs-8">
            <label>下属部门名称</label><br>
            <select name="primaryTablename" id="primaryTablename" class="form-control  validate[required]">
            </select>
            <%--<input type="hidden" id="primaryTablenameval" value="${'#primaryTablename'}">--%>
        </div>
    </div>
</div>

<script>
    function jumpPage() {
        // 第一个下拉框的选择，局级单位的id
        var councilId = $("#source").val();
        // 获取到局级单位的id
        if (null != councilId && "" != councilId) {
            $.getJSON("http://localhost:8088/SpringMybatis/UserController/test2.do?councilId=" + councilId, function (myJSON) {
                var option = "";
                if (myJSON.length > 0) {
                    option += "<option value=''>==请选择类型==</option>";
                    $.each(myJSON, function (index, indexItems) {
                        option += "<option id=" + indexItems.dep_id + ">" + indexItems.dep_name + "</option>";
                    });
                    $("#primaryTablename").html(option);
                } else if (myJSON.length <= 0) {
                    $("#primaryTablename").hide();
                }
            });
        } else {
            $("#primaryTablename").hide();
        }
    }
</script>
</body>
</html>
