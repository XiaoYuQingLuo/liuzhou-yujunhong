<%--
  Created by IntelliJ IDEA.
  User: YuJunHong
  Date: 18/3/7
  Time: 下午2:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="icon" type="image/png"
          href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <title>招商投资库</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>


    <!-- Bootstrap core CSS     -->
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/bootstrap.min.css"
          rel="stylesheet"/>

    <!-- Animation library for notifications   -->
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/animate.min.css"
          rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/light-bootstrap-dashboard.css?v=1.4.0"
          rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/demo.css"
          rel="stylesheet"/>


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/pe-icon-7-stroke.css"
          rel="stylesheet"/>
    <style>
        .query-part-outer{
            border: 1px solid red;
            width: 1163px;
            height: 150px
        }
        .merchant-all{
            border: 1px solid red;
        }
    </style>
</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="blue"
         data-image="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/img/wallhaven-618085.jpg">

        <!--

            Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
            Tip 2: you can also add an image using data-image tag

        -->

        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="http://www.creative-tim.com" class="simple-text">
                    柳 州 招 商
                </a>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/dashboard.jsp">
                        <i class="pe-7s-graph"></i>
                        <p>首页</p>
                    </a>
                </li>
                <li>
                    <a href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/manager.jsp">
                        <i class="pe-7s-user"></i>
                        <p>User Profile</p>
                    </a>
                </li>
                <li>
                    <a href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/table.html">
                        <i class="pe-7s-note2"></i>
                        <p>项目 管理</p>
                    </a>
                </li>
                <li>
                    <a href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/typography.html">
                        <i class="pe-7s-news-paper"></i>
                        <p>个人 通知</p>
                    </a>
                </li>
                <li>
                    <a href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/icons.html">
                        <i class="pe-7s-science"></i>
                        <p>Icons</p>
                    </a>
                </li>
                <li>
                    <%--<a href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/maps.html">--%>
                    <a href="/SpringMybatis/DepartmentController/showDepartments.do">
                        <i class="pe-7s-map-marker"></i>
                        <p>权限 管理</p>
                    </a>
                </li>
                <li>
                    <a href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/notifications.html">
                        <i class="pe-7s-bell"></i>
                        <p>公告 管理</p>
                    </a>
                </li>
                <li class="active-pro">
                    <a href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/upgrade.html">
                        <i class="pe-7s-rocket"></i>
                        <p>Upgrade to PRO</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">柳州</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-dashboard"></i>
                                <p class="hidden-lg hidden-md">Dashboard</p>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-globe"></i>
                                <b class="caret hidden-lg hidden-md"></b>
                                <p class="hidden-lg hidden-md">
                                    5 Notifications
                                    <b class="caret"></b>
                                </p>
                            </a>


                            <ul class="dropdown-menu">
                                <li><a href="#">Notification 1</a></li>
                                <li><a href="#">Notification 2</a></li>
                                <li><a href="#">Notification 3</a></li>
                                <li><a href="#">Notification 4</a></li>
                                <li><a href="#">Another notification</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">
                                <i class="fa fa-search"></i>
                                <p class="hidden-lg hidden-md">Search</p>
                            </a>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="">
                                <p>用户名</p>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <p>
                                    选择
                                    <b class="caret"></b>
                                </p>

                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#">首页</a></li>
                                <li><a href="#">通知</a></li>
                                <li><a href="#">公告</a></li>
                                <li><a href="#">项目管理</a></li>
                                <li><a href="#">工作计划</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                <p>注销</p>
                            </a>
                        </li>
                        <li class="separator hidden-lg"></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="query-part-outer">
            <div class="form-group">
                <form action="${pageContext.request.contextPath}/BussinessController/bussinessSearchByCondition.do" method="post">
                <input type="text" name = "content" class="form-control" placeholder="输入名称查询">
                <button class="btn btn-default btn-sm" type="submit">查询</button>
                </form>
            </div>
        </div>
        <div class="merchant-all" >
            <table class="table table-hover">
                <thead>
                <th>企业名称</th>
                <th>企业所属行业</th>
                <th>引进状态</th>
                <th>企业地址</th>
                <th>加入库的时间</th>
                <th>创建人</th>
                </thead>
                <tbody>
                <c:forEach items="${requestScope.businesses}" var="businesse">
                    <tr>
                        <td>
                                ${businesse.com_name}
                        </td>
                        <td>
                                ${businesse.industry}
                        </td>
                        <td>
                                ${businesse.state}
                        </td>
                        <td>
                                ${businesse.address}
                        </td>
                        <td>
                                ${businesse.time}
                        </td>
                        <td>
                                ${businesse.name}
                        </td>

                    </tr>
                </c:forEach>
                </tbody>
            </table>

        </div>

        <div class="container-fluid">
            <nav class="pull-left">
                <ul>
                    <li>
                        <a href="#">
                            Home
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Company
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Portfolio
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Blog
                        </a>
                    </li>
                </ul>
            </nav>
            <p class="copyright pull-right">
                &copy;
                <script>document.write(new Date().getFullYear())</script>
                <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
            </p>
        </div>
    </footer>

</div>
</div>


</body>

<!--   Core JS Files   -->
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/jquery.3.2.1.min.js"
        type="text/javascript"></script>
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/bootstrap.min.js"
        type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/demo.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        demo.initChartist();

        $.notify({
            icon: 'pe-7s-gift',
            message: "欢迎使用 <b></b> - 柳州招商引资投资平台."

        }, {
            type: 'info',
            timer: 4000
        });

    });
</script>
</html>
