<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/3/9
  Time: 14:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <%--light-bootstrap-dashboard-master/assets/img/favicon.ico--%>
    <link rel="icon" type="image/png" href="../light-bootstrap-dashboard-master/assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <title>个人通知信息</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>


    <!-- Bootstrap core CSS     -->
    <link href="../light-bootstrap-dashboard-master/assets/css/bootstrap.min.css" rel="stylesheet"/>

    <!-- Animation library for notifications   -->
    <link href="../light-bootstrap-dashboard-master/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="../light-bootstrap-dashboard-master/assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="../light-bootstrap-dashboard-master/assets/css/demo.css" rel="stylesheet"/>


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css

" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300

' rel='stylesheet' type='text/css'>
    <link href="../light-bootstrap-dashboard-master/assets/css/pe-icon-7-stroke.css" rel="stylesheet"/>
    <%-- 引入操作框格的CSS --%>
    <link href="../light-bootstrap-dashboard-master/assets/css/permission.css" rel="stylesheet">


    <style>
        .uppage {
            height: 35px;
            width: 80px;
            margin: 0px 300px;
            float: left
        }

        .downpage {
            height: 45px;
            width: 80px;
            margin: 10px 650px;
        }

        .downpage img {
            height: 45px;
            text-align: center;
            line-height: 45px;
        }

        .uppage img {
            height: 45px;
            text-align: center;
            line-height: 45px;
        }
    </style>
</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="../light-bootstrap-dashboard-master/assets/img/sidebar-5.jpg">

        <!--   you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple" -->


        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="http://www.creative-tim.com" class="simple-text">
                    Creative Tim
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/dashboard.jsp">
                        <i class="pe-7s-graph"></i>
                        <p>首页</p>
                    </a>
                </li>
                <li>
                    <a href="user.html">
                        <i class="pe-7s-user"></i>
                        <p>User Profile</p>
                    </a>
                </li>
                <li>
                    <a href="table.html">
                        <i class="pe-7s-note2"></i>
                        <p>项目管理</p>
                    </a>
                </li>
                <li>
                    <a href="typography.html">
                        <i class="pe-7s-news-paper"></i>
                        <p>个人通知</p>
                    </a>
                </li>
                <li>
                    <a href="icons.html">
                        <i class="pe-7s-science"></i>
                        <p>Icons</p>
                    </a>
                </li>
                <li class="active">
                    <a href="../UserController/showAllUsers.do">
                        <i class="pe-7s-map-marker"></i>
                        <p>人员管理</p>
                    </a>
                </li>
                <li>
                    <a href="notifications.html">
                        <i class="pe-7s-bell"></i>
                        <p>公告管理</p>
                    </a>
                </li>
                <li class="active-pro">
                    <a href="upgrade.html">
                        <i class="pe-7s-rocket"></i>
                        <p>Upgrade to PRO</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">部门管理</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-dashboard"></i>
                                <p class="hidden-lg hidden-md">Dashboard</p>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-globe"></i>
                                <b class="caret hidden-sm hidden-xs"></b>
                                <span class="notification hidden-sm hidden-xs">5</span>
                                <p class="hidden-lg hidden-md">
                                    5 Notifications
                                    <b class="caret"></b>
                                </p>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Notification 1</a></li>
                                <li><a href="#">Notification 2</a></li>
                                <li><a href="#">Notification 3</a></li>
                                <li><a href="#">Notification 4</a></li>
                                <li><a href="#">Another notification</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">
                                <i class="fa fa-search"></i>
                                <p class="hidden-lg hidden-md">Search</p>
                            </a>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="">
                                <p>Account</p>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <p>
                                    Dropdown
                                    <b class="caret"></b>
                                </p>

                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                <p>Log out</p>
                            </a>
                        </li>
                        <li class="separator hidden-lg hidden-md"></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <%--<div class="header">
                                <h4 class="title">部门管理</h4>
                            </div>--%>
                            <div class="content table-responsive table-full-width">
                                <form action="${pageContext.request.contextPath}/UserController/searchUser.do"
                                      method="post">

                                    <div class="depSearch" style="float: left">
                                        <input type="text" name="searchText" placeholder="请输入关键字">
                                    </div>
                                    <div class="submit">
                                        <input type="submit" value="查询">
                                    </div>
                                    <%-- 只有超级管理员可以新增用户 --%>
                                    <c:choose>
                                        <c:when test="${sessionScope.account.id == 1}">
                                            <div class="new_dep">
                                                    <%--<a href="../light-bootstrap-dashboard-master/addNewUser.jsp">--%>
                                                <a href="${pageContext.request.contextPath}/UserController/addNewUserPre.do">
                                                    <input type="button" value="新增">
                                                </a>
                                            </div>
                                        </c:when>
                                    </c:choose>
                                </form>
                                <!--展示查询出的部门-->
                                <table class="table table-hover table-striped">
                                    <thead>
                                    <th>序号</th>
                                    <th>姓名</th>
                                    <th>性别</th>
                                    <th>所属单位</th>
                                    <th>所属部门</th>
                                    <th>职位</th>
                                    <th>联系电话</th>
                                    <th>电子邮箱</th>
                                    <c:choose>
                                        <c:when test="${sessionScope.account.id == 1}">
                                            <%-- 只有超级管理员可以查看状态 --%>
                                            <th>状态</th>
                                        </c:when>
                                    </c:choose>
                                    <th>操作</th>
                                    </thead>
                                    <c:forEach items="${requestScope.userList}" var="user" varStatus="status">
                                        <tr>
                                                <%-- 索引值 --%>
                                            <td>${status.count}</td>
                                                <%-- 姓名 --%>
                                            <td>${user.name}</td>
                                                <%-- 性别 --%>
                                            <td>${user.sex}</td>
                                                <%-- 所属单位 --%>
                                            <td>${user.council_name}</td>
                                                <%-- 所属部门 --%>
                                            <td>${user.d_name}</td>
                                                <%-- 职位名称 --%>
                                            <td>${user.p_name}</td>
                                                <%-- 电话 --%>
                                            <td>${user.tel}</td>
                                                <%-- 邮箱地址 --%>
                                            <td>${user.email}</td>
                                                <%-- 状态 --%>
                                            <c:choose>
                                                <c:when test="${sessionScope.account.id == 1}">
                                                    <td>${user.flag eq 0?'已激活':'已注销'}</td>
                                                </c:when>
                                            </c:choose>
                                            <td>
                                                <a href="http://localhost:8088/SpringMybatis/UserController/showUserInfo.do?u_id=${user.u_id}">
                                                        <%--查看/修改--%>
                                                    <c:choose>
                                                        <c:when test="${sessionScope.account.id == 1}">
                                                            修改
                                                        </c:when>
                                                    </c:choose>
                                                    <c:choose>
                                                        <c:when test="${sessionScope.account.id != 1}">
                                                            查看
                                                        </c:when>
                                                    </c:choose>
                                                </a>
                                                    <%-- 注销用户在修改用户页面完成
                                                    &nbsp;
                                                    <a href="http://localhost:8088/SpringMybatis/UserController/deleteUser.do?u_id=${user.u_id}">
                                                        删除
                                                    </a>
                                                    --%>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </table>
                                <%-- 分页 --%>
                                <form action="../UserController/showAllUsers.do" method="post">
                                    <p>当前页：${page.pageNum}，总页数：${page.pages}</p>
                                    <c:choose>
                                        <c:when test="${page.pageNum>1}">
                                            <button name="page" value="${page.prePage}">上一页</button>
                                        </c:when>
                                    </c:choose>
                                    <c:choose>
                                        <c:when test="${page.pageNum<page.pages}">
                                            <button name="page" value="${page.nextPage}">下一页</button>
                                        </c:when>
                                    </c:choose>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="#">
                                Home
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Company
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Portfolio
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Blog
                            </a>
                        </li>
                    </ul>
                </nav>
                <p class="copyright pull-right">
                    &copy;
                    <script>document.write(new Date().getFullYear())</script>
                    <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
                </p>
            </div>
        </footer>


    </div>
</div>


</body>

<script src="../light-bootstrap-dashboard-master/assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../light-bootstrap-dashboard-master/assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="../light-bootstrap-dashboard-master/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="../light-bootstrap-dashboard-master/assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE

"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="../light-bootstrap-dashboard-master/assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

</html>