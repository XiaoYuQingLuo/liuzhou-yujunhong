<%--
  Created by IntelliJ IDEA.
  User: YuJunHong
  Date: 18/3/7
  Time: 下午2:26
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <link rel="icon" type="image/png" href="../light-bootstrap-dashboard-master/assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <title>Light Bootstrap Dashboard by Creative Tim</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>


    <!-- Bootstrap core CSS     -->
    <link href="../light-bootstrap-dashboard-master/assets/css/bootstrap.min.css" rel="stylesheet"/>

    <!-- Animation library for notifications   -->
    <link href="../light-bootstrap-dashboard-master/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="../light-bootstrap-dashboard-master/assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="../light-bootstrap-dashboard-master/assets/css/demo.css" rel="stylesheet"/>


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="../light-bootstrap-dashboard-master/assets/css/pe-icon-7-stroke.css" rel="stylesheet"/>
    <style>
        .alertNotif {
            color: red;
            font-size: 10px;
        }
    </style>
</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="blue"
         data-image="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/img/wallhaven-618085.jpg">

        <!--

            Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
            Tip 2: you can also add an image using data-image tag

        -->

        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="http://www.creative-tim.com" class="simple-text">
                    柳 州 招 商
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/dashboard.jsp">
                        <i class="pe-7s-graph"></i>
                        <p>首页</p>
                    </a>
                </li>
                <li class="active">
                    <a href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/manager.jsp">
                        <i class="pe-7s-user"></i>
                        <p>User Profile</p>
                    </a>
                </li>
                <li>
                    <a href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/table.html">
                        <i class="pe-7s-note2"></i>
                        <p>项目 管理</p>
                    </a>
                </li>
                <li>
                    <a href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/typography.html">
                        <i class="pe-7s-news-paper"></i>
                        <p>个人 通知</p>
                    </a>
                </li>
                <li>
                    <a href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/icons.html">
                        <i class="pe-7s-science"></i>
                        <p>Icons</p>
                    </a>
                </li>
                <li>
                    <%--<a href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/maps.html">--%>
                    <a href="/SpringMybatis/DepartmentController/showDepartments.do">
                        <i class="pe-7s-map-marker"></i>
                        <p>权限 管理</p>
                    </a>
                </li>
                <li>
                    <a href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/notifications.html">
                        <i class="pe-7s-bell"></i>
                        <p>公告 管理</p>
                    </a>
                </li>
                <li class="active-pro">
                    <a href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/upgrade.html">
                        <i class="pe-7s-rocket"></i>
                        <p>Upgrade to PRO</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">User</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-dashboard"></i>
                                <p class="hidden-lg hidden-md">Dashboard</p>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-globe"></i>
                                <b class="caret hidden-sm hidden-xs"></b>
                                <span class="notification hidden-sm hidden-xs">5</span>
                                <p class="hidden-lg hidden-md">
                                    5 Notifications
                                    <b class="caret"></b>
                                </p>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Notification 1</a></li>
                                <li><a href="#">Notification 2</a></li>
                                <li><a href="#">Notification 3</a></li>
                                <li><a href="#">Notification 4</a></li>
                                <li><a href="#">Another notification</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">
                                <i class="fa fa-search"></i>
                                <p class="hidden-lg hidden-md">Search</p>
                            </a>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="">
                                <p>Account</p>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <p>
                                    Dropdown
                                    <b class="caret"></b>
                                </p>

                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                <p>Log out</p>
                            </a>
                        </li>
                        <li class="separator hidden-lg hidden-md"></li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">新增 用户</h4>
                            </div>
                            <div class="content">
                                <%--<form action="${pageContext.request.contextPath}/UserController/addNewUser.do" method="post">--%>
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>登录名</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="alertNotif" id="inputLoginAccount"></span>
                                            <input type="text" class="form-control" placeholder="登录名"
                                                   name="loginAccount" id="loginAccount" required="required">
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>登录密码</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="alertNotif" id="inputLoginPass"></span>
                                            <input type="text" class="form-control" placeholder="登录密码"
                                                   name="loginPass" id="loginPass" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>确认登录密码</label>
                                            <input type="text" class="form-control" placeholder="确认登录密码"
                                                   name="confirmLoginPass" id="confirmLoginPass" required="required">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>姓名</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="alertNotif" id="inputName"></span>
                                            <input type="text" required="required" class="form-control" placeholder="姓名"
                                                   name="name" id="name">
                                        </div>
                                    </div>
                                </div>
                                <%-- 显示局级单位、部门和职位的下拉框 --%>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>所属局</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="alertNotif" id="inputCouncil"></span>
                                            <select required="required" name="council" id="council" class="form-control"
                                                    onchange="jumpPage()"><%--source--%>
                                                <option value="">===请选择===</option>
                                                <c:forEach items="${councilList}" var="list">
                                                    <option value="${list.id}">
                                                        <span style="white-space:pre"></span>
                                                            ${list.council_name}
                                                        <span style="white-space:pre"></span>
                                                    </option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>所属部门</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="alertNotif" id="inputDepartment"></span>
                                            <select required="required" name="department" id="department"
                                                    class="form-control"><%--primaryTablename--%>
                                            </select>
                                        </div>
                                    </div>
                                    <%--可以不选择职位，默认新增用户都是职员，需要更改职位需进入用户管理中修改--%>
                                    <%--<div class="col-md-4">
                                        <div class="form-group">
                                            <label>职位</label>

                                            <select required="required" name="position" id="position"
                                                    class="form-control">
                                                <option value="">===请选择===</option>
                                                <c:forEach items="${positionList}" var="list">
                                                    <option value="${list.id}">
                                                        <span style="white-space:pre"></span>
                                                            ${list.p_name}
                                                        <span style="white-space:pre"></span>
                                                    </option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>--%>
                                </div>

                                <span class="alertNotif" id="buttonAlert"></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <%--<button type="submit" class="btn btn-info btn-fill pull-right">--%>
                                <button class="btn btn-info btn-fill pull-right" onclick="addNewUser()">
                                    确认新增
                                </button>
                                <div class="clearfix"></div>
                                <%--</form>--%>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-user">
                            <div class="image">
                                <img src="https://ununsplash.imgix.net/photo-1431578500526-4d9613015464?fit=crop&fm=jpg&h=300&q=75&w=400"
                                     alt="..."/>
                            </div>
                            <div class="content">
                                <div class="author">
                                    <a href="#">
                                        <img class="avatar border-gray" src="assets/img/faces/face-3.jpg" alt="..."/>

                                        <h4 class="title">Mike Andrew<br/>
                                            <small>michael24</small>
                                        </h4>
                                    </a>
                                </div>
                                <p class="description text-center"> "Lamborghini Mercy <br>
                                    Your chick she so thirsty <br>
                                    I'm in that two seat Lambo"
                                </p>
                            </div>
                            <hr>
                            <div class="text-center">
                                <button href="#" class="btn btn-simple"><i class="fa fa-facebook-square"></i></button>
                                <button href="#" class="btn btn-simple"><i class="fa fa-twitter"></i></button>
                                <button href="#" class="btn btn-simple"><i class="fa fa-google-plus-square"></i>
                                </button>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="#">
                                Home
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Company
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Portfolio
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Blog
                            </a>
                        </li>
                    </ul>
                </nav>
                <p class="copyright pull-right">
                    &copy;
                    <script>document.write(new Date().getFullYear())</script>
                    <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
                </p>
            </div>
        </footer>

    </div>
</div>


</body>
<%-- 局级单位和部门下拉框的二级联动JS代码 --%>
<script src="../light-bootstrap-dashboard-master/assets/js/addNewUser.js"></script>

<!--   Core JS Files   -->
<script src="../light-bootstrap-dashboard-master/assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../light-bootstrap-dashboard-master/assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="../light-bootstrap-dashboard-master/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="../light-bootstrap-dashboard-master/assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="../light-bootstrap-dashboard-master/assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="../light-bootstrap-dashboard-master/assets/js/demo.js"></script>



<%--<script>
    function jumpPage() {
        alert("二级联动？");
        // 第一个下拉框的选择，局级单位的id
        var councilId = $("#council").val();
        // 获取到局级单位的id
        if (null != councilId && "" != councilId) {
            $.getJSON("http://localhost:8088/SpringMybatis/UserController/getDepList.do?councilId=" + councilId, function (myJSON) {
                var option = "";
                if (myJSON.length > 0) {
                    option += "<option value=''>==请选择部门==</option>";
                    $.each(myJSON, function (index, indexItems) {
                        option += "<option value=" + indexItems.dep_id + ">" + indexItems.dep_name + "</option>";
                    });
                    $("#department").html(option);
                } else if (myJSON.length <= 0) {
                    $("#department").hide();
                }
            });
        } else {
            $("#department").hide();
        }
    }

    function addNewUser() {
        alert("进入新增方法");
        var inputLoginAccount = $("#inputLoginAccount");
        var inputLoginPass = $("#inputLoginPass");
        var buttonAlert = $("#buttonAlert");
        // 每次点击按钮都清空提示信息
        inputLoginAccount.html("");
        inputLoginPass.html("");
        buttonAlert.html("");
        // 检查是否都已经填写内容--->后台还应该验证
        /*var flag = isAllGiven();
        if (!flag) {
            // 有至少一项没有填写，则终止函数的执行
            alert("请补全信息！");
            return;
        }*/
        /*使用AJAX传参*/
        $.ajax({
            url: "http://localhost:8088/SpringMybatis/UserController/addNewUser.do",
            type: "post",
            dataType: "json",
            data: {
                loginAccount: $("#loginAccount").val(),
                loginPass: $("#loginPass").val(),
                confirmLoginPass: $("#confirmLoginPass").val(),
                name: $("#name").val(),
                councilId: $("#council").val(),
                departmentId: $("#department").val(),
                positionId: $("#position").val()
            },
            success: function (data) {
                alert(data);
                /*
                1-表示用户名重复
                2-表示用户名长度不合法
                3-表示登录密码长度不合法
                4-表示两次密码不一致
                5-写入account表失败（新增失败）
                6-写入user表失败
                7-新增成功
                 */
                if (data == 1) {
                    /*alert("登录名重复");*/
                    inputLoginAccount.html("登录名已存在");
                } else if (data == 2) {
                    /*alert("登录名长度不合法");*/
                    inputLoginAccount.html("登录名长度在5-30位");
                } else if (data == 3) {
                    /*alert("登录密码长度不合法");*/
                    inputLoginPass.html("登录密码长度在5-32位");
                } else if (data == 4) {
                    /*alert("两次密码输入不一致");*/
                    inputLoginPass.html("两次密码输入不一致");
                } else if (data == 5) {
                    /*alert("新增失败，请联系维护商");*/
                    buttonAlert.html("新增失败，请联系维护商");
                } else if (data == 6) {
                    /*alert("新增失败，请联系维护商");*/
                    buttonAlert.html("新增失败，请联系维护商");
                } else {
                    alert("新增成功");
                    // 返回上一级页面
                    window.history.back(-1);
                }
                // 无论是否新增成功，都清空输入框
                $("#loginAccount").val("");
                $("#loginPass").val("");
                $("#confirmLoginPass").val("");
                $("#name").val("");
                $("#council").val("");
                $("#department").val("");
            }
        });
    }

    /*
    检查是否所有必要的信息都已经由用户输入
     */
    /*function isAllGiven {
        var loginAccount = $("#loginAccount");
        var loginPass = $("#loginPass");
        var confirmLoginPass = $("#confirmLoginPass");
        var name = $("#name");
        var council = $("#council");
        var department = $("#department");
        if ((loginAccount != null && loginAccount != "") && (loginPass != null && loginPass != "")
            && (confirmLoginPass != null && confirmLoginPass != "") && (name != null && name != "")
            && (council != null && council != "") && (department != null && department != "")) {
            // 必要信息都给出
            return true;
        }
        return false;
    }*/
</script>--%>
</html>
