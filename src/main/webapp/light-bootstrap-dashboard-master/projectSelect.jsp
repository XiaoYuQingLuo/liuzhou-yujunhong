<%--
  Created by IntelliJ IDEA.
  User: YuJunHong
  Date: 18/3/8
  Time: 下午2:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="icon" type="image/png"
          href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <title>Light Bootstrap Dashboard by Creative Tim</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>


    <!-- Bootstrap core CSS     -->
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/bootstrap.min.css"
          rel="stylesheet"/>

    <!-- Animation library for notifications   -->
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/animate.min.css"
          rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/light-bootstrap-dashboard.css?v=1.4.0"
          rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/demo.css"
          rel="stylesheet"/>


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/pe-icon-7-stroke.css"
          rel="stylesheet"/>
    <title>事项查询</title>
    <style>
        .projectSearch input {
            height: 55px;
            width: 500px;
            margin-top: 50px;
            margin-left: 400px;
            font-size: 15px;
            font-family: "Songti SC";
            text-align: center;
            background-color: rgba(255, 255, 255, 0.7);
            border: none;
            box-shadow: rgba(0, 0, 0, 0.3) 2px 3px 2px;
        }

        .submit input {
            height: 55px;
            width: 100px;
            margin: 50px 0px;
            font-size: 15px;
            font-family: "Songti SC";
            background-color: rgba(13, 23, 255, 0.2);
            box-shadow: rgba(0, 0, 0, 0.3) 2px 3px 2px;
        }
    </style>
</head>
<body>
<form action="${pageContext.request.contextPath}/ProjectSelectController/projectSelect.do" method="post">

    <div class="projectSearch" style="float: left">
        <input type="text" name="projectSelect" placeholder="请输入关键字">
    </div>
    <div class="submit">
        <input type="submit" value="查询">
    </div>
</form>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">事项查询</h4>
                        <p class="category" style="float: left">结果</p>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <%-- 使用JSTL遍历在request中存储的list集合中的数据 --%>
                        <table class="table table-hover table-striped">
                            <thead>
                            <th>编号</th>
                            <th>工程名</th>
                            <th>开始时间</th>
                            <th>结束时间</th>
                            <th>资金</th>
                            <th>用地</th>
                            </thead>
                            <tbody>
                            <c:forEach items="${requestScope.projectSelects}" var="projectSelect" varStatus="status">
                                <tr>
                                        <%-- 索引值 --%>
                                    <td>${status.count}</td>
                                    <td>
                                        <a href="http://localhost:8088/SpringMybatis/ProjectSelectController/projectInfor.do?id=${projectSelect.id}">${projectSelect.pro_name}</a>
                                    </td>
                                    <td>${projectSelect.start_time}</td>
                                    <td>${projectSelect.finish_time}</td>
                                    <td>${projectSelect.funds}</td>
                                    <td>${projectSelect.ground}</td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<!--   Core JS Files   -->
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/jquery.3.2.1.min.js"
        type="text/javascript"></script>
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/bootstrap.min.js"
        type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>

</html>
