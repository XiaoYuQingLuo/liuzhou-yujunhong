function jumpPage() {
    /*alert("二级联动？");*/
    // 第一个下拉框的选择，局级单位的id
    var councilId = $("#council").val();
    // 获取到局级单位的id
    if (null != councilId && "" != councilId) {
        $.getJSON("http://localhost:8088/SpringMybatis/UserController/getDepList.do?councilId=" + councilId, function (myJSON) {
            var option = "";
            if (myJSON.length > 0) {
                option += "<option value=''>==请选择部门==</option>";
                $.each(myJSON, function (index, indexItems) {
                    option += "<option value=" + indexItems.dep_id + ">" + indexItems.dep_name + "</option>";
                });
                $("#department").html(option);
            } else if (myJSON.length <= 0) {
                $("#department").hide();
            }
        });
    } else {
        $("#department").hide();
    }
}

function addNewUser() {
    /*alert("进入新增方法");*/
    var inputLoginAccount = $("#inputLoginAccount");
    var inputLoginPass = $("#inputLoginPass");
    var buttonAlert = $("#buttonAlert");
    // 每次点击按钮都清空提示信息
    inputLoginAccount.html("");
    inputLoginPass.html("");
    buttonAlert.html("");
    /*使用AJAX传参*/
    $.ajax({
        url: "http://localhost:8088/SpringMybatis/UserController/addNewUser.do",
        type: "post",
        dataType: "json",
        data: {
            loginAccount: $("#loginAccount").val(),
            loginPass: $("#loginPass").val(),
            confirmLoginPass: $("#confirmLoginPass").val(),
            name: $("#name").val(),
            councilId: $("#council").val(),
            departmentId: $("#department").val(),
            positionId: $("#position").val()
        },
        success: function (data) {
            /*alert(data);*/
            /*
            1-表示用户名重复
            2-表示用户名长度不合法
            3-表示登录密码长度不合法
            4-表示两次密码不一致
            5-写入account表失败（新增失败）
            6-写入user表失败
            7-新增成功
             */
            if (data == 0) {
                // 信息补全
                buttonAlert.html("请补全信息！");
            } else if (data == 1) {
                /*alert("登录名重复");*/
                inputLoginAccount.html("登录名已存在");
            } else if (data == 2) {
                /*alert("登录名长度不合法");*/
                inputLoginAccount.html("登录名长度在5-30位");
            } else if (data == 3) {
                /*alert("登录密码长度不合法");*/
                inputLoginPass.html("登录密码长度在5-32位");
            } else if (data == 4) {
                /*alert("两次密码输入不一致");*/
                inputLoginPass.html("两次密码输入不一致");
            } else if (data == 5) {
                /*alert("新增失败，请联系维护商");*/
                buttonAlert.html("新增失败，请联系维护商");
            } else if (data == 6) {
                /*alert("新增失败，请联系维护商");*/
                buttonAlert.html("新增失败，请联系维护商");
            } else {
                alert("新增成功");
                // 返回上一级页面
                window.history.back(-1);
            }
            // 无论是否新增成功，都清空输入框
            $("#loginAccount").val("");
            $("#loginPass").val("");
            $("#confirmLoginPass").val("");
            $("#name").val("");
            $("#council").val("");
            $("#department").val("");
        }
    });
}
