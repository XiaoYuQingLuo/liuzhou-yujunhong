<%--
  Created by IntelliJ IDEA.
  User: YuJunHong
  Date: 18/3/9
  Time: 上午10:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="icon" type="image/png"
          href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <title>Light Bootstrap Dashboard by Creative Tim</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>


    <!-- Bootstrap core CSS     -->
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/bootstrap.min.css"
          rel="stylesheet"/>

    <!-- Animation library for notifications   -->
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/animate.min.css"
          rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/light-bootstrap-dashboard.css?v=1.4.0"
          rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/demo.css"
          rel="stylesheet"/>


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/pe-icon-7-stroke.css"
          rel="stylesheet"/>
    <title>Title</title>
    <style>
        .projectInfor{
            background-color: #FFFFFF;
            border: 1px solid #E3E3E3;
            border-radius: 4px;
            color: #565656;
            padding: 8px 12px;
            height: 40px;
            -webkit-box-shadow: none;
            box-shadow: none;
        }
    </style>
</head>
<body>
<c:forEach items="${requestScope.projectInforLists}" var="projectInforList" >
<div class="content" >
    <div class="container-fluid" >
        <div class="row" >
            <div class="col-md-8">
                <div class="card" style="width: 1340px">
                    <div class="header">
                        <h4 class="title">事项详情</h4>
                    </div>
                    <div class="content" >
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>工程名</label>
                                    </div>
                                    <div class="projectInfor">
                                            ${projectInforList.pro_name}
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>事项级别</label>
                                    </div>
                                    <div class="projectInfor">
                                            ${projectInforList.pro_type}
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>审核状态</label>
                                    </div>
                                    <div class="projectInfor">
                                            ${projectInforList.pro_state}
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>事项类型</label>
                                    </div>
                                    <div class="projectInfor">
                                            ${projectInforList.pro_level}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>开始时间</label>
                                    </div>
                                    <div class="projectInfor">
                                            ${projectInforList.start_time}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label >结束时间</label>
                                    </div>
                                    <div class="projectInfor">
                                            ${projectInforList.finish_time}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>资金</label>
                                    </div>
                                    <div class="projectInfor">
                                            ${projectInforList.funds}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>用地</label>
                                    </div>
                                    <div class="projectInfor">
                                            ${projectInforList.ground}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>创建时间</label>
                                    </div>
                                    <div class="projectInfor">
                                            ${projectInforList.build_time}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>更新时间</label>
                                    </div>
                                    <div class="projectInfor">
                                            ${projectInforList.update_time}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>工程内容</label>
                                    </div>
                                    <div class="projectInfor">
                                            ${projectInforList.pro_text}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>工程内容</label>
                                    </div>
                                    <div class="projectInfor" style="height: 100px">
                                            ${projectInforList.pro_text}
                                    </div>
                                </div>
                            </div>
                        <c:choose>
                            <c:when test="${(user.p_id == 1) && (projectInforList.pro_state == '未审核')}">
                                        <form action="../ProjectController/councilPass.do">
                                            <button type="submit" class="btn btn-info btn-fill pull-right">审核通过</button>
                                        </form>
                                        <form action="../ProjectController/councilFail.do">
                                            <button type="submit" class="btn btn-info btn-fill pull-right">审核不通过</button>
                                        </form>
                            </c:when>
                        </c:choose>

                            <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            </c:forEach>
</body>
<!--   Core JS Files   -->
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/jquery.3.2.1.min.js"
        type="text/javascript"></script>
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/bootstrap.min.js"
        type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>
</html>
