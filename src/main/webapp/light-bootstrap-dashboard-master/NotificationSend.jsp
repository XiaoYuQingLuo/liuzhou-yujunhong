<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/3/9
  Time: 14:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <%--light-bootstrap-dashboard-master/assets/img/favicon.ico--%>
    <link rel="icon" type="image/png" href="../light-bootstrap-dashboard-master/assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <title>Light Bootstrap Dashboard by Creative Tim</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>


    <!-- Bootstrap core CSS     -->
    <link href="../light-bootstrap-dashboard-master/assets/css/bootstrap.min.css" rel="stylesheet"/>

    <!-- Animation library for notifications   -->
    <link href="../light-bootstrap-dashboard-master/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="../light-bootstrap-dashboard-master/assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="../light-bootstrap-dashboard-master/assets/css/demo.css" rel="stylesheet"/>


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css

" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300

' rel='stylesheet' type='text/css'>
    <link href="../light-bootstrap-dashboard-master/assets/css/pe-icon-7-stroke.css" rel="stylesheet"/>
    <%-- 引入操作框格的CSS --%>
    <link href="../light-bootstrap-dashboard-master/assets/css/permission.css" rel="stylesheet">


    <style>
        .uppage {
            height: 35px;
            width: 80px;
            margin: 0px 300px;
            float: left
        }

        .downpage {
            height: 45px;
            width: 80px;
            margin: 10px 650px;
        }

        .downpage img {
            height: 45px;
            text-align: center;
            line-height: 45px;
        }

        .uppage img {
            height: 45px;
            text-align: center;
            line-height: 45px;
        }
    </style>
</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="blue"
         data-image="../light-bootstrap-dashboard-master/assets/img/wallhaven-618085.jpg">

        <!--   you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple" -->


        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="http://www.creative-tim.com" class="simple-text">
                    柳州招商引资
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="../AccountController/index.do">
                        <i class="pe-7s-graph"></i>
                        <p>首页</p>
                    </a>
                </li>
                <li>
                    <a href="manager.jsp">
                        <i class="pe-7s-user"></i>
                        <p>用户管理</p>
                    </a>
                </li>
                <li class="">
                    <a href="table.html">
                        <i class="pe-7s-note2"></i>
                        <p>项目 管理</p>
                    </a>
                </li>
                <li>
                    <a href="typography.html">
                        <i class="pe-7s-news-paper"></i>
                        <p>个人 通知</p>
                    </a>
                </li>
                <li>
                    <a href="icons.html">
                        <i class="pe-7s-science"></i>
                        <p>Icons</p>
                    </a>
                </li>
                <li>
                    <a href="maps.html">
                        <i class="pe-7s-map-marker"></i>
                        <p>Maps</p>
                    </a>
                </li>
                <li>
                    <a href="notifications.html">
                        <i class="pe-7s-bell"></i>
                        <p>公告 管理</p>
                    </a>
                </li>
                <li class="active-pro">
                    <a href="upgrade.html">
                        <i class="pe-7s-rocket"></i>
                        <p>Upgrade to PRO</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">User</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-dashboard"></i>
                                <p class="hidden-lg hidden-md">Dashboard</p>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-globe"></i>
                                <b class="caret hidden-sm hidden-xs"></b>
                                <span class="notification hidden-sm hidden-xs">5</span>
                                <p class="hidden-lg hidden-md">
                                    5 Notifications
                                    <b class="caret"></b>
                                </p>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Notification 1</a></li>
                                <li><a href="#">Notification 2</a></li>
                                <li><a href="#">Notification 3</a></li>
                                <li><a href="#">Notification 4</a></li>
                                <li><a href="#">Another notification</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">
                                <i class="fa fa-search"></i>
                                <p class="hidden-lg hidden-md">Search</p>
                            </a>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="">
                                <p>Account</p>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <p>
                                    Dropdown
                                    <b class="caret"></b>
                                </p>

                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                <p>Log out</p>
                            </a>
                        </li>
                        <li class="separator hidden-lg hidden-md"></li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">发送信息</h4>
                        </div>
                        <form action="../NotificationController/SendNotification.do" method="post"
                              enctype="multipart/form-data">
                            <div class="content">
                                <div class="row">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>主题</label><br>
                                                <input type="text" name="notif_title" id="notif_title"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <label>收件人</label><br>
                                        <!--指定 date标记-->
                                        <select name="recieve_user_id" id="recieve_user_id" multiple>
                                            <c:forEach items="${sessionScope.AllUser}" var="allUser">
                                                <option value="${allUser.u_id}">${allUser.d_name}:${allUser.name}</option>
                                            </c:forEach>
                                        </select><br>
                                    </div>
                                </div>
                                <div class="page-header">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>内容</label>
                                                <textarea rows="5" class="form-control" id="notif_text"
                                                          name="notif_text"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <table>
                                        <tr>
                                            <td>文件上传：</td>
                                            <td><input type="file" name="photo" id="files"></td>
                                        </tr>
                                    </table>
                                    <button type="submit" class="btn btn-info btn-fill pull-right"
                                            onClick="return fun()">发送
                                    </button>
                                </div>
                            </div>
                        </form>
                        <a href="../NotificationController/AllNotifcationInfo.do">返回</a>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer">
    <div class="container-fluid">
        <nav class="pull-left">
            <ul>
                <li>
                    <a href="#">
                        Home
                    </a>
                </li>
                <li>
                    <a href="#">
                        Company
                    </a>
                </li>
                <li>
                    <a href="#">
                        Portfolio
                    </a>
                </li>
                <li>
                    <a href="#">
                        Blog
                    </a>
                </li>
            </ul>
        </nav>
        <p class="copyright pull-right">
            &copy;
            <script>document.write(new Date().getFullYear())</script>
            <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
        </p>
    </div>
</footer>
</div>

</div>

<script>
</script>
</body>
<script type="text/javascript">
    function fun() {
        var title = document.getElementById("notif_title");
        var text = document.getElementById("notif_text");
        var userId = document.getElementById("recieve_user_id");
        var fileSize = document.getElementById("files");
        alert(fileSize);
        if (title.value == "" || title.value == null) {
            alert("主题不能为空");
            return false;
        }
        if (text.value == "" || text.value == null) {
            alert("内容不能为空");
            return false;
        }
        if (userId.value == "" || userId.value == null) {
            alert("请选择收件人");
            return false;
        }
        if (fileSize.file[0].size != "" || fileSize.file[0].size != null) {
            if (fileSize > 10000) {
                alert("文件过大");
                return false;
            }
        }
        return true;
    }
</script>


<script src="../light-bootstrap-dashboard-master/assets/js/jquery.3.2.1.min.js"

        type="text/javascript"></script>
<script src="../light-bootstrap-dashboard-master/assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="../light-bootstrap-dashboard-master/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="../light-bootstrap-dashboard-master/assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE

"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="../light-bootstrap-dashboard-master/assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

</html>

