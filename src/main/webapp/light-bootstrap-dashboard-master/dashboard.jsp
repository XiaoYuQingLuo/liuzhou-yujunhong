<%--
  Created by IntelliJ IDEA.
  User: YuJunHong
  Date: 18/3/7
  Time: 下午2:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="icon" type="image/png"
          href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <title>柳州招商投资</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>


    <!-- Bootstrap core CSS     -->
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/bootstrap.min.css"
          rel="stylesheet"/>

    <!-- Animation library for notifications   -->
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/animate.min.css"
          rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/light-bootstrap-dashboard.css?v=1.4.0"
          rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/demo.css"
          rel="stylesheet"/>


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/css/pe-icon-7-stroke.css"
          rel="stylesheet"/>
    <style>
        .information {
            border-bottom: 1px #f2d8ff solid;
            height: 50px;
            font-size: 15px;
            line-height: 50px;
            text-align: center;

        }
    </style>
</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="blue"
         data-image="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/img/wallhaven-618085.jpg">

        <!--

            Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
            Tip 2: you can also add an image using data-image tag

        -->

        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="http://www.creative-tim.com" class="simple-text">
                    柳 州 招 商
                </a>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/dashboard.jsp">
                        <i class="pe-7s-graph"></i>
                        <p>首页</p>
                    </a>
                </li>
                <li>
                    <a href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/manager.jsp">
                        <i class="pe-7s-user"></i>
                        <p>User Profile</p>
                    </a>
                </li>
                <li>
                    <a href="../ProjectController/table2.do">
                        <i class="pe-7s-note2"></i>
                        <p>项目 管理</p>
                    </a>
                </li>
                <li>
                    <a href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/typography.html">
                        <i class="pe-7s-news-paper"></i>
                        <p>个人 通知</p>
                    </a>
                </li>
                <li>
                    <a href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/icons.html">
                        <i class="pe-7s-science"></i>
                        <p>Icons</p>
                    </a>
                </li>
                <li>
                    <a href="/SpringMybatis/DepartmentController/showDepartments.do">
                        <i class="pe-7s-map-marker"></i>
                        <p>部门 管理</p>
                    </a>
                </li>
                <%-- 新增标签，人员管理 --%>
                <li>
                    <a href="/SpringMybatis/UserController/showAllUsers.do">
                        <i class="pe-7s-map-marker"></i>
                        <p>人员 管理</p>
                    </a>
                </li>
                <li>
                    <a href="/SpringMybatis/BussinessController/bussinessService.do">
                        <i class="pe-7s-map-marker"></i>
                        <p>招商 引资</p>
                    </a>
                </li>
                <li>
                    <a href="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/notifications.html">
                        <i class="pe-7s-bell"></i>
                        <p>公告 管理</p>
                    </a>
                </li>
                <c:choose>
                    <c:when test="${account.id eq 1}">
                        </li>
                        <a href="http://localhost:8088/SpringMybatis/OperateLogController/SelectAll.do">
                            <i class="pe-7s-map-marker"></i>
                            <p>操作日志</p>
                        </a>
                        </li>
                    </c:when>
                </c:choose>

                <li class="active-pro">
                    <a href="http://localhost:8088/SpringMybatis/PlanController/myPlan.do">
                        <i class="pe-7s-rocket"></i>
                        <p>计划管理</p>
                    </a>

            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">柳州</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-dashboard"></i>
                                <p class="hidden-lg hidden-md">Dashboard</p>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-globe"></i>
                                <b class="caret hidden-lg hidden-md"></b>
                                <p class="hidden-lg hidden-md">
                                    5 Notifications
                                    <b class="caret"></b>
                                </p>
                            </a>


                            <ul class="dropdown-menu">
                                <li><a href="#">Notification 1</a></li>
                                <li><a href="#">Notification 2</a></li>
                                <li><a href="#">Notification 3</a></li>
                                <li><a href="#">Notification 4</a></li>
                                <li><a href="#">Another notification</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">
                                <i class="fa fa-search"></i>
                                <p class="hidden-lg hidden-md">Search</p>
                            </a>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="">


                                <div>${user.name}</div>


                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <p>
                                    选择
                                    <b class="caret"></b>
                                </p>

                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#">首页</a></li>
                                <li><a href="#">通知</a></li>
                                <li><a href="#">公告</a></li>
                                <li><a href="#">项目管理</a></li>
                                <li><a href="#">工作计划</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                <p>注销</p>
                            </a>
                        </li>
                        <li class="separator hidden-lg"></li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">

                            <div class="header">
                                <h4 class="title">个人通知</h4>
                                <p class="category">最新通知</p>
                            </div>

                            <div class="content">
                                <div class="ct-chart ct-perfect-fourth">
                                    <c:choose>
                                        <c:when test="${sessionScope.NotificationInfo==null}">
                                            没有新的信息
                                            <a href="../NotificationController/AllNotifcationInfo.do">更多信息</a>
                                        </c:when>
                                    </c:choose>

                                    <c:forEach items="${sessionScope.NotificationInfo }" var="Notification"
                                               varStatus="status">
                                        <div class="information">
                                            <a href="../NotificationController/NotificationInfo/ ${Notification.id}.do">
                                                    ${Notification.notif_title}
                                            </a>
                                        </div>
                                    </c:forEach>

                                </div>

                                <div class="footer">
                                    <div class="legend"></div>
                                    <hr>
                                    <div class="stats">
                                        <a href="../NotificationController/AllNotifcationInfo.do">更多信息</a>
                                        <i class="fa fa-clock-o"></i> 实时更新
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">我的事项</h4>
                                <p class="category">最新事项</p>
                            </div>
                            <div class="content">
                                <div class="ct-chart">
                                    <c:forEach items="${sessionScope.myProjects}" var="myProject">
                                        <div class="information">${myProject.pro_name}</div>
                                    </c:forEach>
                                </div>
                                <div class="footer">
                                    <div class="legend">
                                    </div>
                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-history"></i> 实时更新
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="card ">
                            <div class="header">
                                <h4 class="title">我的待办事项</h4>
                                <p class="category">未处理的事项</p>
                            </div>
                            <div class="content">
                                <div class="ct-chart">
                                    <c:forEach items="${sessionScope.unreadProjects}" var="unreadProject">
                                        <div class="information">${unreadProject.pro_name}</div>
                                    </c:forEach>


                                </div>

                                <div class="footer">
                                    <div class="legend">

                                    </div>
                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-check"></i> 数据信息认证
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card ">
                            <div class="header">
                                <h4 class="title">即将逾期项目</h4>
                                <p class="category">请尽快处理</p>
                            </div>
                            <div class="content">
                                <div class="table-full-width">
                                    <table class="table">
                                        <tbody>
                                        <c:forEach items="${sessionScope.overTimeProjects}" var="overTimeProject">
                                            <tr>
                                                <td>${overTimeProject.pro_name}</td>

                                                <td class="td-actions text-right">
                                                    <button type="button" rel="tooltip" title="Edit Task"
                                                            class="btn btn-info btn-simple btn-xs">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                    <button type="button" rel="tooltip" title="Remove"
                                                            class="btn btn-danger btn-simple btn-xs">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="footer">
                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-history"></i> 实时更新
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="#">
                                Home
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Company
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Portfolio
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Blog
                            </a>
                        </li>
                    </ul>
                </nav>
                <p class="copyright pull-right">
                    &copy;
                    <script>document.write(new Date().getFullYear())</script>
                    <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
                </p>
            </div>
        </footer>

    </div>
</div>


</body>

<!-- Core JS Files -->
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/jquery.3.2.1.min.js"
        type="text/javascript"></script>
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/bootstrap.min.js"
        type="text/javascript"></script>

<!-- Charts Plugin -->
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/chartist.min.js"></script>

<!-- Notifications Plugin -->
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/bootstrap-notify.js"></script>

<!-- Google Maps Plugin -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="http://localhost:8088/SpringMybatis/light-bootstrap-dashboard-master/assets/js/demo.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        demo.initChartist();

        $.notify({
            icon: 'pe-7s-gift',
            message: "欢迎使用 <b></b> - 柳州招商引资投资平台."

        }, {
            type: 'info',
            timer: 4000
        });

    });
</script>
</html>
