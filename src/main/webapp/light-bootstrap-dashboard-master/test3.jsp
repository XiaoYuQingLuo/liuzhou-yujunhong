<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/3/15
  Time: 15:23
  To change this template use File | Settings | File Templates.
--%>
<%--测试AJAX传参--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Insert title here</title>
    <link href="../bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet" media="screen" />
    <link href="../bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen"/>
    <script src="../jquery/jquery-1.8.3.min.js"></script>
    <script src="../bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script src="../bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js"></script>
    <script src="../bootstrap-datetimepicker-master/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
</head>
<body>

<div class="form-group">
    <div class="input-group date form_datetime col-md-5" data-date="1979-09-16" data-date-format="yyyy-MM-dd" data-link-field="dtp_input1">
        <input id="test" type="text" name="test" />
        <a>
            <button onclick="test();">提交</button>
        </a>
    </div>

</div>


<script>
    function test() {
        $.ajax({
            url:"http://localhost:8088/SpringMybatis/UserController/test.do",
            type:"post",
            dataType:"json",
            data:{
                content:$("#test").val()
            },
            success:function (data) {
                if(data == 1){
                    alert("测试成功！");
                }
            }
        });
    }
</script>
</body>


</html>
