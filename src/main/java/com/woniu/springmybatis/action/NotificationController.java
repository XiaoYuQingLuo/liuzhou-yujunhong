package com.woniu.springmybatis.action;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.woniu.springmybatis.action.UserOperateLog.UserOperate;
import com.woniu.springmybatis.entity.User;
import com.woniu.springmybatis.entity.Vi_Notif_User;
import com.woniu.springmybatis.entity.Vi_account_user_depatment_position_council;
import com.woniu.springmybatis.service.NotificationService;
import com.woniu.springmybatis.service.UploadService;
import com.woniu.springmybatis.service.UserService;
import com.woniu.springmybatis.service.Vi_account_user_depatment_position_councilService;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;


import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 个人信息的Controller
 */
@Controller
//窄化地址
@RequestMapping("/NotificationController")
public class NotificationController {
    @Resource
    private NotificationService notificationService;
    @Resource
    private UserService userService;
    @Resource
    private Vi_account_user_depatment_position_councilService
            vi_account_user_depatment_position_councilService;
    @Resource
    private UploadService fileUpload;

    /**
     * 通过页面点击具体的个人信息查看里面的详情
     *
     * @param id
     * @param session
     * @return
     */
    @RequestMapping(value = "/NotificationInfo/{id}.do")
    @UserOperate(module_name = "findNotificationByND",option = "查看信息的详情")
    public String findNotificationByND(@PathVariable(name = "id") Integer id, HttpSession session) {

        // 通过信息的id查找信息的详细信息 查找的是试图
        Vi_Notif_User vi_notif_user = this.notificationService.notifIDFindNotif(id);
        // 信息详情保存在session中
        session.setAttribute("NotificationInfoByNotificationID", vi_notif_user);
        // 点击信息后，直接修改成已读
        if (vi_notif_user.getNotif_state().equals(2)) {
            this.notificationService.changStateID(id);
        }
        //调转到页面
        return "NotificationByID1";
    }

    /**
     * 通过userID查找全部信息
     * 查找的是视图
     * @param session
     * @return
     */

//    public String NotificationAllInfo(HttpSession session) {
//        //通过用户的id查找全部的信息
//        User user = (User) session.getAttribute("user");
//        List<Vi_Notif_User> Vi_Notif_Users = this.notificationService.idFindNotif(user.getId());
//        session.setAttribute("AllNotificationInfo",Vi_Notif_Users);
//        return "Notifications";
//
//    }

    /**
     * 个人信息分页功能
     * 通过userID查找全部信息
     * 查找的是视图
     */
    @RequestMapping("/AllNotifcationInfo.do")
    @UserOperate(option = "查看个人信息",module_name = "notificationShow")
    public String notificationShow(HttpSession session,
                                   @RequestParam(required = true, defaultValue = "1") int notifiPage) {
        //设置分页的开始页和每页显示数量
        PageHelper.startPage(notifiPage, 8);
        // 获取用户id
        User user = (User) session.getAttribute("user");
        //获取个人信息
        List<Vi_Notif_User> vi_notif_users = this.notificationService.idFindNotif(user.getId());
        // 获取全部信息
        List<Vi_account_user_depatment_position_council> vi_account_user_depatment_position_councils
                = this.vi_account_user_depatment_position_councilService.findAllthing(user.getId());

        PageInfo<Vi_Notif_User> vi_notif_userPageInfo = new PageInfo<Vi_Notif_User>(vi_notif_users);
        //存储信息
        session.setAttribute("AllNotificationInfo", vi_notif_users);
        //存储分页
        session.setAttribute("notifiPage", vi_notif_userPageInfo);
        // 查询收件人
        session.setAttribute("AllUser", vi_account_user_depatment_position_councils);
        return "Notifications";

    }

    /**
     * 发送信息
     *
     * @param session
     * @param recieve_user_id
     * @param notif_title
     * @param notif_text
     * @return
     */
    @RequestMapping("/SendNotification.do")
    @UserOperate(module_name = "sendNotification" ,option = "发送信息")
    public String sendNotification(HttpSession session,
                                   Model model, @RequestParam(required = true) Integer[] recieve_user_id,
                                   @RequestParam(required = true) String notif_title,
                                   @RequestParam(required = true) String notif_text,
                                   MultipartFile photo) {
        System.out.println("photo是："+photo);
        User user = (User) session.getAttribute("user");

        int i = 0;
        //上传文件信息
        if(photo!=null) {
            this.fileUpload.fileUpload(photo);
        }
        for (Integer userid : recieve_user_id) {
            i = this.notificationService.insertNotifi(userid, user.getId(), notif_title, notif_text);
        }
        if (i > 0) {
            return "redirect:../NotificationController/AllNotifcationInfo.do";
        }
        return "dashboard";
    }
}
