package com.woniu.springmybatis.action.UserOperateLog;

import java.lang.annotation.*;

/**
 * 自定义UserOperate注解
 */

@Retention(RetentionPolicy.RUNTIME)//在运行时有效
@Target(ElementType.METHOD)
@Documented
public @interface UserOperate {
    /**操作方法名*/
    String module_name () default "";
    /**操作内容*/
    String option() default "";
}
