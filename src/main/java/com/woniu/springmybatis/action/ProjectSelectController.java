package com.woniu.springmybatis.action;

import com.woniu.springmybatis.action.UserOperateLog.UserOperate;
import com.woniu.springmybatis.entity.AllProjectInformations;
import com.woniu.springmybatis.entity.Project;
import com.woniu.springmybatis.service.ProjectSelectService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author YuJunHong
 */
@Controller
@RequestMapping("/ProjectSelectController")
public class ProjectSelectController {
    @Resource
    private ProjectSelectService projectSelectService;


    @UserOperate(option = "查询项目",module_name ="projectSelect" )
    @RequestMapping("/projectSelect.do")
    public String projectSelect(Model model,javax.servlet.http.HttpServletRequest httpServletRequest) {
        String content=httpServletRequest.getParameter("projectSelect");
        System.out.println(content);
        // 传输数据到jsp中，会放置在request中
        List<AllProjectInformations> projectList = this.projectSelectService.validateProjectInfor(content);
        model.addAttribute("projectSelects", projectList);
        return "projectSelect";
    }
    @UserOperate(option = "项目详情",module_name = "projectInfor")
    @RequestMapping("/projectInfor.do")
    public String projectInfor(Model model, javax.servlet.http.HttpServletRequest httpServletRequest,
                               HttpSession session){
        String projectId=httpServletRequest.getParameter("id");
        session.setAttribute("pid",projectId);
        List<AllProjectInformations> projectInforLists = this.projectSelectService.validateProjectInformations(projectId);
        model.addAttribute("projectInforLists", projectInforLists);
        return "projectInfor";
    }
}
