package com.woniu.springmybatis.action;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.woniu.springmybatis.action.UserOperateLog.UserOperate;
import com.woniu.springmybatis.entity.Plan;
import com.woniu.springmybatis.entity.PlanInformation;
import com.woniu.springmybatis.entity.User;
import com.woniu.springmybatis.service.PlanService;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;


@Controller
@RequestMapping("/PlanController")
public class PlanController {

    @Resource
    private PlanService planService;

    /**
     * 新增计划
     * @param plan
     * @param session
     * @return
     */
    @RequestMapping("/newPlan.do")
    @UserOperate(option = "新增计划",module_name = "newPlan")
    public String newPlan(Plan plan,HttpSession session){
        User user = (User)session.getAttribute("user");
        // 将当前用户id和部门id添加入信息
        plan.setPlan_user_id(user.getId());
        plan.setPlan_dep_id(user.getD_id());
        this.planService.newPlan(plan);
        return "redirect:myPlan.do";
    }

    /**访问我的计划页面时根据用户id查询计划信息
     *
     * @param session
     * @param mv
     * @param page
     * @param pageSize
     * @return
     */
    @UserOperate(module_name = "myPlan",option = "查看我的计划")
    @RequestMapping("/myPlan.do")
    public String myPlan(HttpSession session, Model mv ,
                         @RequestParam(required = true,defaultValue = "1") Integer page,
                         @RequestParam(required = false,defaultValue = "4") Integer pageSize
                         ){
        PageHelper.startPage(page,pageSize);
        User user = (User)session.getAttribute("user");
        List<PlanInformation> plans = planService.showPlan(user.getId());
        PageInfo<PlanInformation> p = new PageInfo<PlanInformation>(plans);
        mv.addAttribute("plans",plans);
        mv.addAttribute("page",p);
        return "myplan";
    }

    /** 根据计划id删除计划
     *
     * @param id
     * @return
     */
    @UserOperate(option = "删除计划",module_name = "deletePlan")
    @RequestMapping("/deletePlan.do")
    public String deletePlan(int id){
        this.planService.deletePlan(id);
        return "redirect:myPlan.do";
    }

    /**根据id展示计划详情
     *
     * @param id
     * @param model
     * @return
     */
    @UserOperate(module_name = "showPlan",option = "计划详情")
    @RequestMapping("/showPlan.do")
    public String showPlan(int id,Model model){
        PlanInformation planInformation = this.planService.findPlanInformation(id);
        model.addAttribute("plan",planInformation);
        return "showplan";
    }
}
