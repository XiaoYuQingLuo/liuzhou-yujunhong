package com.woniu.springmybatis.action;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.woniu.springmybatis.action.UserOperateLog.UserOperate;
import com.woniu.springmybatis.entity.*;
import com.woniu.springmybatis.mapper.ProjectSelectMapper;
import com.woniu.springmybatis.service.*;
import com.woniu.springmybatis.tools.UpLoad;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static java.lang.Integer.parseInt;

@Controller
@RequestMapping("/ProjectController")
public class ProjectController {

    @Resource
    private ProjectService projectService;

    @Resource
    private ProjectSelectService projectSelectService;

    @Resource
    private Vi_project_user_stateService vi_project_user_stateService;

    @Resource
    private Pro_levelService pro_levelService;
    @Resource
    private CreateMissionService createMissionService;
    @Resource
    private ProjectFilePathService projectFilePathService;

    /**
     * 展示部门
     *
     * @param model
     * @return
     */
    @RequestMapping("/table.do")
    public String showDepartment(Model model) {
        model.addAttribute("business", this.projectService.findAllBusiness());
        model.addAttribute("level", this.pro_levelService.findAll());
        return "table1";
    }

    /**
     * 查看项目
     *
     * @param model
     * @param session
     * @param page
     * @param pageSize
     * @return
     */
    @RequestMapping("/table2.do")
    @UserOperate(option = "查看项目", module_name = "showProject")
    public String showProject(Model model, HttpSession session, @RequestParam(required = true, defaultValue = "1") Integer page,
                              @RequestParam(required = false, defaultValue = "3") Integer pageSize) {
        PageHelper.startPage(page, pageSize);
        List<Vi_project_user_state> list = this.vi_project_user_stateService.findAll();
        PageInfo<Vi_project_user_state> pageInfo = new PageInfo<>(list);
        session.setAttribute("project", list);
        session.setAttribute("page", pageInfo);
        return "table";
    }

    @RequestMapping("/councilPass.do")
    public String councilPass(HttpSession session) {
        String projectId = (String) session.getAttribute("pid");
        System.out.println(projectId);
        this.projectSelectService.changeCouncilPass(projectId);
        return "table";
    }

    @RequestMapping("/councilFail.do")
    public String councilFail(HttpSession session) {
        String projectId = (String) session.getAttribute("pid");
        System.out.println(projectId);
        this.projectSelectService.changeCouncilFail(projectId);
        return "table";
    }

    @RequestMapping(value = "/upload2.do", method = RequestMethod.POST)
    public String upLoadFile(HttpServletRequest request, Files file_Path,HttpSession session) {
        try {
            // 获取工程id最大值
            Integer maxId = this.createMissionService.selectProjectMaxId();
            // 时间格式转换
            SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yy-MM-dd hh:mm:ss");
            String nowTime = simpleDateFormat.format(new Date());
            // 获取user，通过user
            User user = (User) session.getAttribute("user");
            // 获取文件名和文件路径
            List<String> lists = UpLoad.upload2(request);
            // 添加实体类数据
            file_Path.setName(lists.get(1));
            file_Path.setAddress(lists.get(2));
            file_Path.setProject_id(maxId);
            file_Path.setTime(nowTime);
            file_Path.setUser_id(user.getId());
            this.projectFilePathService.filePathSave(file_Path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "table1";
    }
}
