package com.woniu.springmybatis.action;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.woniu.springmybatis.entity.Account;
import com.woniu.springmybatis.entity.Vi_Notif_User;
import com.woniu.springmybatis.entity.Vi_user_dep_council__position_operatelog;
import com.woniu.springmybatis.mapper.Vi_user_dep_council__position_operatelogMapper;
import com.woniu.springmybatis.service.Vi_account_user_depatment_position_councilService;
import com.woniu.springmybatis.service.Vi_user_dep_council__position_operatelogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

import static com.sun.xml.internal.ws.api.model.wsdl.WSDLBoundOperation.ANONYMOUS.required;

/**
 * 用户操作日志
 */
@Controller
@RequestMapping("/OperateLogController")
public class OperateLogController {
    @Resource
    private Vi_user_dep_council__position_operatelogService
            vi_user_dep_council__position_operatelogService;

    @RequestMapping("/SelectAll.do")
    public String selectAllInfo(HttpSession session, Model model,
                                @RequestParam(required = true, defaultValue = "1") int operateLogsPage) {
       //设置分页
        PageHelper.startPage(operateLogsPage, 8);
        List<Vi_user_dep_council__position_operatelog> operatelogs =
                this.vi_user_dep_council__position_operatelogService.selcetAllInfo();
        PageInfo<Vi_user_dep_council__position_operatelog> PageInfo =
                new PageInfo<Vi_user_dep_council__position_operatelog>(operatelogs);
        model.addAttribute("operatelogs", operatelogs);
        session.setAttribute("OperateLogsPage", PageInfo);

        return "OperateLog";

    }
}
