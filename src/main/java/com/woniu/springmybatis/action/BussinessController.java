package com.woniu.springmybatis.action;

import com.woniu.springmybatis.action.UserOperateLog.UserOperate;
import com.woniu.springmybatis.entity.Vi_bussiness_state_user;
import com.woniu.springmybatis.service.BussinessService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/BussinessController")
public class BussinessController {
    @Resource
    private BussinessService bussinessService;

    /**
     * 用于展示招商引资库所有数据的Controller
     * @param model
     * @return
     */
    @UserOperate(module_name = "bussinessController",option = "展示招商引资库")
    @RequestMapping("/bussinessService.do")
    public String bussinessController(Model model){
        List<Vi_bussiness_state_user> businesses = bussinessService.bussinessSelect();
        //String,Object,
        model.addAttribute("businesses",businesses);
        return "merchant";
    }

    /**
     *查询招商引资库
     * @param model
     * @param content
     * @return
     */
    @UserOperate(module_name = "SearchByConditionController",option = "展示招商引资库")
    @RequestMapping("/bussinessSearchByCondition.do")
    public String SearchByConditionController(Model model, @RequestParam(name="content") String content){
        System.out.println(content+"====>在Controller里获得的Content");
        List<Vi_bussiness_state_user> list = bussinessService.SearchByCondition(content);
        model.addAttribute("businesses",list);
        return  "merchant";
    }


}
