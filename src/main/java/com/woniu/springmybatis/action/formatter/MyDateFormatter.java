package com.woniu.springmybatis.action.formatter;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.format.Formatter;

/**
 * 实现类型转换，将String转换为Date 接口Formatter需要泛型，即目标数据类型
 *
 * @author tbths
 *
 */
public class MyDateFormatter implements Formatter<Date> {

	@Override
	// 将响应的目标数据类型转换为String
	public String print(Date arg0, Locale arg1) {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

		return format.format(arg0);
	}

	@Override
	// 将请求中的数据类型转换为目标数据类型
	public Date parse(String arg0, Locale arg1) throws ParseException {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

		return format.parse(arg0);
	}

}