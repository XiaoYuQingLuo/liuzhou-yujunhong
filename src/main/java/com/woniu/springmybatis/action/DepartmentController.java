package com.woniu.springmybatis.action;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.woniu.springmybatis.action.UserOperateLog.UserOperate;
import com.woniu.springmybatis.entity.*;
import com.woniu.springmybatis.service.DepartmentService;
import com.woniu.springmybatis.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/DepartmentController")
public class DepartmentController {
	@Resource
	private DepartmentService departmentService;
	@Resource
	private UserService userService;

	/**
	 * 查看部门
	 * @param model
	 * @param page
	 * @param pageSize
	 * @return
	 */
	@RequestMapping("/showDepartments.do")
	@UserOperate(option = "查看部门",module_name = "showDepartments")
	public String showDepartments(Model model, @RequestParam(required = true, defaultValue = "1") Integer page,
								  @RequestParam(required = false, defaultValue = "8") Integer pageSize) {
		// 开始分页
		PageHelper.startPage(page, pageSize);
		List<Vi_user_dep_council> deps = this.departmentService.showAllDep();
		// 将查询结果保存进分页中
		PageInfo<Vi_user_dep_council> p = new PageInfo<>(deps);
		for (int i = 0; i < deps.size(); i++) {
			System.out.println(deps.get(i));
		}
		model.addAttribute("deps", deps);
		model.addAttribute("page", p);
		return "depManage";
	}

	/**
	 * 搜索部门
	 *
	 * @param model
	 * @param httpServletRequest
	 * @return
	 */
	@RequestMapping("/searchDep.do")
	@UserOperate(option = "查看部门",module_name = "searchDep")
	public String searchDep(Model model, HttpServletRequest httpServletRequest, @RequestParam(required = true, defaultValue = "1") Integer page,
							@RequestParam(required = false, defaultValue = "8") Integer pageSize) {
		// System.out.println("测试");
		// 获取用户在搜索框中输入的数据
		PageHelper.startPage(page, pageSize);
		String content = httpServletRequest.getParameter("searchText");
		System.out.println("用户输入数据是：" + content);
		if (content.equals("")) {
			// 如果用户并没有输入数据(空字符串)，则刷新界面
			/*model.addAttribute("notInput",1);*/
			/*return showDepartments(model,);*/
		}
		List<Vi_user_dep_council> deps = this.departmentService.searchDep(content);
		PageInfo<Vi_user_dep_council> p = new PageInfo<>(deps);
		// 将数据库中搜索到的结果存储到request中
		model.addAttribute("deps", deps);
		model.addAttribute("page", p);
		return "depManage";
	}

	/**
	 * 查看部门详情
	 *
	 * @param model
	 * @param httpServletRequest
	 */
	@UserOperate(module_name = "showDepInfo",option = "查看部门详情")
	@RequestMapping("/showDepInfo.do")
	public String showDepInfo(Model model, HttpServletRequest httpServletRequest) {
		// 得到部门Id
		String dep_id = httpServletRequest.getParameter("dep.id");
		System.out.println("用户选择的部门id是：" + dep_id);

		// 调用service中方法得到部门详情
		// 1-得到具体部门名称、所属局和部门领导和部门简介
		Vi_user_dep_council dep = this.departmentService.getDep(dep_id);
		System.out.println(dep);
		// 2-得到具体部门的下属员工的基本信息
		List<Vi_account_user_depatment_position_council> depUsers = this.departmentService.getDepUser(dep_id);

		/*System.out.println("======================");*/
		for (int i = 0; i < depUsers.size(); i++) {
			System.out.println(depUsers.get(i));
		}
		model.addAttribute("depInfo", dep);
		model.addAttribute("dep_users", depUsers);
		return "depDetails";
	}

	/**
	 * 修改部门信息
	 *
	 * @param model
	 * @param httpServletRequest
	 */
	@RequestMapping("/updateDepInfo.do")
	@UserOperate(option = "修改部门信息",module_name = "updateDepInfo")
	public String updateDepInfo(Model model, HttpServletRequest httpServletRequest) {
		// 部门id
		String d_id = httpServletRequest.getParameter("d_id");
		// 转换类型
		Integer id = Integer.valueOf(d_id);
		// 部门名称
		String d_name = httpServletRequest.getParameter("d_name");
		// 部门简介
		String d_text = httpServletRequest.getParameter("d_text");
		System.out.println("部门id是：" + d_id);
		System.out.println("部门简介是：" + d_text);
		System.out.println("部门名称是：" + d_name);
		// 封装为POJO对象
		Department dep = new Department();
		dep.setId(id);
		dep.setD_name(d_name);
		dep.setD_text(d_text);
		boolean flag = this.departmentService.updateDepInfo(dep);

		/*
		如何完成更新之后直接显示最新的部门信息的功能
		 */
		return "dashboard";
	}

	/**
	 * 删除具体部门
	 * 需要注意，部门下没有任何员工才能删除部门，否则会出错
	 *
	 * @param model
	 * @param httpServletRequest
	 * @return
	 */
	@UserOperate(module_name = "deleteDep",option = "删除具体部门")
	@RequestMapping("/deleteDep.do")
	public String deleteDep(Model model, HttpServletRequest httpServletRequest) {
		// 得到部门id
		String dep_id = httpServletRequest.getParameter("dep.id");
		System.out.println("用户选择的部门id是：" + dep_id);
		int row = this.departmentService.deleteDep(dep_id);
		if (row == 1) {
			System.out.println("删除成功");
			return "dashboard";
		}
		return "dashboard";
	}

	/**
	 * 新增部门的前期准备工作，需要获取局级单位list和员工list
	 *
	 * @return
	 */
	@RequestMapping("/addNewDepPre.do")
	@UserOperate(option = "新增部门的前期准备工作",module_name = "addNewDepPre")
	public String addNewDepPre(Model model) {
		System.out.println("新增部门控制器进来了！");
		// 得到局级单位list
		List<Council> councilList = this.departmentService.getCouncilList();
		// 得到基层员工list
		List<User> userList = this.departmentService.getStaffList();
		model.addAttribute("councilList", councilList);
		model.addAttribute("userList", userList);
		return "addNewDep";
	}

	/**
	 * 获取用户输入的数据，新增部门
	 *
	 * @param model
	 * @return
	 */
	@UserOperate(option = "新增部门",module_name = "addNewDep")
	@RequestMapping("/addNewDep.do")
	public String addNewDep(Model model, HttpServletRequest httpServletRequest) {
		// 获取用户数据
		// 部门名称
		String dep_name = httpServletRequest.getParameter("dep_name");
		// 部门简介
		String dep_text = httpServletRequest.getParameter("dep_text");
		// 所属局级单位id
		String council_id = httpServletRequest.getParameter("council_id");
		// 格式转换
		Integer c_id = Integer.valueOf(council_id);
		Department dep = new Department();
		dep.setCouncil_id(c_id);
		dep.setD_name(dep_name);
		dep.setD_text(dep_text);
		// 部门领导id
		String leader = httpServletRequest.getParameter("leader");
		Integer leader_id = Integer.valueOf(leader);
		System.out.println(dep_name + '\t' + dep_text + '\t' + council_id + '\t' + leader_id);
		// 新增部门（部门名称、部门简介、部门所属局），需要获取新增之后的部门id
		int newDepId = this.departmentService.addNewDep(dep);
		System.out.println("新增部门的id是：" + newDepId);
		// 确定部门领导
		int row = this.userService.setDepLeader(c_id, newDepId, leader_id);
		System.out.println(row);
		return "dashboard";
	}
}
