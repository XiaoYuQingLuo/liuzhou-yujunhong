package com.woniu.springmybatis.action;

import com.woniu.springmybatis.action.UserOperateLog.UserOperate;
import com.woniu.springmybatis.entity.Department;
import com.woniu.springmybatis.entity.Mission;
import com.woniu.springmybatis.service.CreateMissionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author YuJunHong
 */
@Controller
@RequestMapping("CreateMissionController")
public class CreateMissionController {
    @Resource
    private CreateMissionService createMissionService;
    @UserOperate(option = "新建任务",module_name = "missionSave")
    @RequestMapping("/missionSave.do")
    public String missionSave(Mission mission){
        System.out.println(createMissionService.selectProjectMaxId());
        mission.setProject_id(createMissionService.selectProjectMaxId());
        mission.setFinish_state(1);
        mission.setFlag(0);
        createMissionService.creatMission(mission);
        return "table";
    }
    @UserOperate(option = "新增子任务",module_name = "addMissionSave")
    @RequestMapping("/addMissionSave.do")
    public String addMissionSave(Mission mission){
        System.out.println(createMissionService.selectProjectMaxId());
        mission.setProject_id(createMissionService.selectProjectMaxId());
        mission.setFinish_state(1);
        mission.setFlag(0);
        createMissionService.creatMission(mission);
        return "redirect:../CreateMissionController/selectDepartment.do";
    }
    @UserOperate(option = "查询部门",module_name = "selectDepartment")
    @RequestMapping("/selectDepartment.do")
    public String selectDepartment(Model model){
        List<Department> departments=createMissionService.SelectDepartment();
        model.addAttribute("departments",departments);
        return "createmission";

    }

}
