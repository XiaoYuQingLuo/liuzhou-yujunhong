package com.woniu.springmybatis.action;

import com.woniu.springmybatis.action.UserOperateLog.UserOperate;
import com.woniu.springmybatis.entity.*;
import com.woniu.springmybatis.service.*;
import com.woniu.springmybatis.service.AccountService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/AccountController")
public class AccountController {
    @Resource
    private AccountService accountService;
    @Resource
    private NotificationService notificationService;
    @Resource
    private UserService userService;
    @Resource
    private MyProjectService myProjectService;
    @Resource
    private UnreadProjectService unreadProjectService;
    @Resource
    private OverTimeProjectService overTimeProjectService;
    //    @RequestMapping("/index.do")
//    public ModelAndView index(ModelAndView modelAndView){
//        // 传输数据到jsp中，会放置在request中
//        modelAndView.addObject("users",this.userService.findAll());
//        modelAndView.setViewName("success");
//    return modelAndView;
//    }
    // 首页


    @RequestMapping("/index.do")
    @UserOperate(module_name ="/index" ,option = "登陆")
    public String index(HttpSession session) {

        // 传输数据到jsp中，会放置在request中
        /*
		List<Account> accounts = this.userService.findAll();
		modelAndView.addAttribute("users", this.userService.findAll());
		*/
        Account account = (Account) session.getAttribute("account");


        // 如果list为空的话则直接使用捕获，并且返回登录页面
        try {
            User user = this.userService.findUserByAccountID(account.getId());
            System.out.println("登陆用户的id是："+user.getId());
            session.setAttribute("user", user);
            // 未读通知
            List<Vi_Notif_User> notifications = this.notificationService.useridFindNotif(user.getId());
            session.setAttribute("NotificationInfo", notifications);
            // 我的事项
            List<Project> myProjects=this.myProjectService.validateMyProject(user.getId());
            session.setAttribute("myProjects",myProjects);
            // 我的未读事项
            List<Project> unreadProjects=this.unreadProjectService.validateUnreadProject(user.getId());
            session.setAttribute("unreadProjects",unreadProjects);
            // 即将逾期的项目
            List<Project> overTimeProjects=this.overTimeProjectService.validateOverTimeProject(user.getId());
            session.setAttribute("overTimeProjects",overTimeProjects);
        } catch (NullPointerException e) {
            return "redirect:/cover.html";
        }
        return "dashboard";
    }

    @RequestMapping("/delete.do")
    public String delete(int id) {
        this.accountService.deleteById(id);
        // 回到用户最新界面
        return "redirect:index.do";
    }

    @RequestMapping("/register.do")
    public void register(Account account, HttpServletResponse response) throws IOException {
        this.accountService.saveUserInfor(account);
        response.sendRedirect("../index.jsp");
    }
}
