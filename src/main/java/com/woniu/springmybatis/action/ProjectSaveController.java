package com.woniu.springmybatis.action;

import com.woniu.springmybatis.action.UserOperateLog.UserOperate;
import com.woniu.springmybatis.entity.Account;
import com.woniu.springmybatis.entity.Project;
import com.woniu.springmybatis.entity.User;
import com.woniu.springmybatis.service.ProjectSaveService;
import com.woniu.springmybatis.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/ProjectSaveController")
public class ProjectSaveController {

    @Resource
    private ProjectSaveService projectSaveService;

    @Resource
    private UserService userService;


    @RequestMapping("/ProjectSave.do")
    @UserOperate(option = "新建项目",module_name ="projectSelect" )
    public String projectSelect(HttpSession session, ModelAndView modelAndView, Project project){
        // 从session中取得当前用户的id
        User user = (User)session.getAttribute("user");
        // 设置当前用户id
        project.setUser_id(user.getId());

        // 从数据库中搜索到领导的id
        project.setInspector_id(this.userService.findLeader(user.getId()).getId());

        // 添加项目状态 ， 默认为1 未审核
        project.setPro_state(1);
        // 获取系统当前时间，传入更新时间这一列
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = df.format(System.currentTimeMillis());
        project.setUpdate_time(date);

        // 调用service层的保存方法，判断是否保存成功
        if(this.projectSaveService.saveProject(project)){
            return "redirect:../CreateMissionController/selectDepartment.do";
        }else {
            return "table1";
        }
    }
}
