package com.woniu.springmybatis.action;

import com.woniu.springmybatis.action.UserOperateLog.UserOperate;
import com.woniu.springmybatis.entity.Account;
import com.woniu.springmybatis.entity.User;
import com.woniu.springmybatis.service.AccountService;
import com.woniu.springmybatis.service.NotificationService;
import com.woniu.springmybatis.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

/**
 * @author Taylor
 */
@Controller
/**
 * 窄化url地址
 */
@RequestMapping("/LoginController")
public class LoginController {
    @Resource
    private AccountService accountService;//账户信息
    @Resource
    private UserService userService;//用户信息
    @Resource
    private NotificationService notificationService;//个人信息


    //    @RequestMapping("/login.do")
    //    public ModelAndView login(Account account, HttpSession session,ModelAndView modelAndView){
    //        // 调用服务层代码验证
    //        Account user1 = this.userService.validateUserInfor(account.getRole_name(), account.getNote());
    //        //结果
    //        if (user1!=null){
    //            session.setAttribute("account",user1);
    //            // 跳转到UserController的index.do中
    //
    //        }
    //        else {
    //            modelAndView.setViewName("failed");
    //        }
    //        return modelAndView;
    @RequestMapping("/login.do")
    public String login(Account account, HttpSession session, ModelAndView modelAndView) {
        // 调用服务层代码验证
        Account login_account = this.accountService.validateUserInfor(account.getUsername(), account.getPassword());
        //结果
        if (login_account != null) {
            // 登陆成功，在session中存储登录的用户个人信息
            User user = this.userService.findUserByAccountID(login_account.getId());
              session.setAttribute("account", login_account);
            // 跳转到UserController的index.do中,重定向-->改变url地址，相对路径
            return "redirect:../AccountController/index.do";
        } else {
            // 返回视图，逻辑视图名
            return "redirect:/jsp/failed.jsp";
        }
    }
}
