package com.woniu.springmybatis.service;

import com.woniu.springmybatis.entity.Project;
import com.woniu.springmybatis.mapper.OvertTimeProjectMapper;
import com.woniu.springmybatis.mapper.UnreadProjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author YuJunHong
 */
@Service("OverTimeProjectService")
@Transactional(readOnly = true)
public class OverTimeProjectService {
    @Autowired
    private OvertTimeProjectMapper overtTimeProjectMapper;

    public List<Project> validateOverTimeProject(Integer id) {
        List<Project> overTimeProjects=new ArrayList<>();
        Example example = new Example(Project.class);
        // 组织查询条件---->使用andcondition
        example.createCriteria().andCondition("user_id=", id);
        // 查询所有的本人事项
        List<Project> allProjects = overtTimeProjectMapper.selectByExample(example);
        for (Project overTimeProject : allProjects
                ) {
            // 获取事项开始时间
            String startTime = overTimeProject.getStart_time();
            // 获取事项结束时间
            String finishTime = overTimeProject.getFinish_time();
            java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd");
            try {
                java.util.Date beginDate= format.parse(startTime);
                java.util.Date endDate= format.parse(finishTime);
                long day=(endDate.getTime()-beginDate.getTime())/(24*60*60*1000);
                if (day<=2){
                    overTimeProjects.add(overTimeProject);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        // 三元表达式，返回overTimeProjects对象
        return overTimeProjects != null && !overTimeProjects.isEmpty() ? overTimeProjects : null;
    }
}
