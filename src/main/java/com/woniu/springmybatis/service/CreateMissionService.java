package com.woniu.springmybatis.service;

import com.woniu.springmybatis.entity.Department;
import com.woniu.springmybatis.entity.Mission;
import com.woniu.springmybatis.mapper.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author YuJunHong
 */
@Service("CreateMissionService")
public class CreateMissionService {
    @Resource
    private CreateMissionMapper createMissionMapper;
    @Resource
    private DepartmentMapper departmentMapper;

    @Resource
    private ProjectMaxIdMapper projectMaxIdMapper;
    @Resource
    private MissionMaxIdMapper missionMaxIdMapper;
    // 插入子任务数据
    public void creatMission(Mission mission) {
        this.createMissionMapper.insert(mission);
    }

    // 查询部门名称
    public List<Department> SelectDepartment() {
        return this.departmentMapper.selectAll();

    }

    // 获取工程id的最大值
    public Integer selectProjectMaxId() {
        return this.projectMaxIdMapper.getMaxId();
    }
    // 获取子任务id的最大值
    public Integer getMaxId(){
        return  this.missionMaxIdMapper.getMaxId();
    }
}
