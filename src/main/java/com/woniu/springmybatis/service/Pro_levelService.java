package com.woniu.springmybatis.service;

import com.woniu.springmybatis.entity.Pro_level;
import com.woniu.springmybatis.mapper.Pro_levelMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("Pro_levelService")
public class Pro_levelService {

    @Resource
    private Pro_levelMapper pro_levelMapper;

    public List<Pro_level> findAll(){
        return this.pro_levelMapper.selectAll();
    }
}
