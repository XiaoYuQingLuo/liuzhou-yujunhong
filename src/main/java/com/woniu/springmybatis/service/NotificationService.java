package com.woniu.springmybatis.service;


import com.woniu.springmybatis.action.UserOperateLog.UserOperate;
import com.woniu.springmybatis.entity.Notification;
import com.woniu.springmybatis.entity.Vi_Notif_User;
import com.woniu.springmybatis.mapper.NotificationMapper;
import com.woniu.springmybatis.mapper.Vi_Notif_UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

/**
 * 个人通知业务层
 */
@Service("notificationService")
@Transactional(readOnly = true)
public class NotificationService {
    @Autowired
    private NotificationMapper notificationMapper;
    //查询详细信息 查询的是个人信息的视图
    @Autowired
    private Vi_Notif_UserMapper vi_notif_userMapper;

    /**
     * 使用收件人个人ID查找
     * 用户个人信息的全部内容查询
     * 包含已查看和未查看的信息
     *
     * @param id
     * @return
     */
    public List<Vi_Notif_User> idFindNotif(Integer id) {

        //按条件查询
        Example example = new Example(Notification.class);
        example.setOrderByClause("send_time DESC");
        //更新收件人的id查询
        example.createCriteria().andCondition("recieve_user_id=", id)
                .andCondition("flag=", 0);

        //通过收件人id查询返回集合
        List<Vi_Notif_User> vi_notif_users = this.vi_notif_userMapper.selectByExample(example);
        //如果notifications不是空，notifications不是实体，那么返回notifications集合或者返回空
        return vi_notif_users != null && !vi_notif_users.isEmpty() ? vi_notif_users : null;

    }

    /**
     * 使用收件人个人ID查找
     * 用户个人信息的全部内容查询
     * 不包含已读的信息
     *查询的是个人通知视图
     * @param id
     * @return
     */

    public List<Vi_Notif_User> useridFindNotif(Integer id) {

        //按条件查询
        Example example = new Example(Notification.class);
        //更新收件人的id查询
        //2是未读状态
        // 排序 更新发送时间排序，页面显示最多5条信息
        example.setOrderByClause("send_time DESC limit 0,5 ");
        //查询语句
        example.createCriteria().andCondition("recieve_user_id=", id)
                .andCondition("flag=", 0).
                andCondition("notif_state=", 2);
        //通过收件人id查询返回集合
        List<Vi_Notif_User> vi_notif_users = this.vi_notif_userMapper.selectByExample(example);
        //如果notifications不是空，notifications不是实体，那么返回notifications集合或者返回空
        return vi_notif_users != null && !vi_notif_users.isEmpty() ? vi_notif_users : null;

    }

    /**
     * 使用信息的id查找详细信息
     *查询的是个人信息视图
     * @param id
     * @return
     */
    public Vi_Notif_User notifIDFindNotif(Integer id) {
        //按条件查询
        Example example = new Example(Notification.class);
        //更新收件人的id查询
         example.createCriteria().andCondition("id=", id)
                .andCondition("flag=", 0);
        List<Vi_Notif_User> notiInfo = this.vi_notif_userMapper.selectByExample(example);
        return notiInfo != null && !notiInfo.isEmpty() ? notiInfo.get(0) : null;
    }

    /**
     * 信息查看后修改成已读状态
     */
    @Transactional(readOnly = false)
    public int changStateID(Integer id) {
        //设置修改条件
        Notification notification = new Notification();
        notification.setId(id);
        notification.setNotif_state(1);
        //修改
        int i = this.notificationMapper.updateByPrimaryKeySelective(notification);
        //返回值
        return i;
    }
    /**
     * 发送信息
     *
     */
    @Transactional(readOnly = false)
    public int insertNotifi(Integer recieve_user_id, Integer send_id, String notif_title, String notif_text){

        Notification notification = new Notification();
        // 收件人
        notification.setRecieve_user_id(recieve_user_id);
        //发送人
        notification.setSend_user_id(send_id);
        //信息主题
        notification.setNotif_title(notif_title);
        //信息内容
        notification.setNotif_text(notif_text);
        // 发送时间

        notification.setSend_time(new Date());
        int i = this.notificationMapper.insertSelective(notification);
        return i;
    }


}
