package com.woniu.springmybatis.service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.woniu.springmybatis.entity.Vi_project_user_state;
import com.woniu.springmybatis.mapper.Vi_project_user_stateMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("vi_project_user_stateService")
public class Vi_project_user_stateService {
    @Resource
    private Vi_project_user_stateMapper vi_project_user_stateMapper;
    public List<Vi_project_user_state> findAll(){
        List<Vi_project_user_state> states = this.vi_project_user_stateMapper.selectAll();
        return states;
    }


}
