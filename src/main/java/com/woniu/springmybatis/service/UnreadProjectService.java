package com.woniu.springmybatis.service;

import com.woniu.springmybatis.entity.Project;
import com.woniu.springmybatis.mapper.MyProjectMapper;
import com.woniu.springmybatis.mapper.UnreadProjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @author YuJunHong
 */
@Service("UnreadProjectService")
@Transactional(readOnly = true)
public class UnreadProjectService {
    @Autowired
    private UnreadProjectMapper unreadProjectMapper;

    public List<Project> validateUnreadProject(Integer id) {
        Example example = new Example(Project.class);
        // 组织查询条件---->使用andcondition
        example.createCriteria().andCondition("user_id=", id).andCondition("pro_state=",1);

        // 使用通用mapper中的按条件查询
        List<Project> unreadProjects=this.unreadProjectMapper.selectByExample(example);
        // 三元表达式，返回Account对象
        return unreadProjects != null && !unreadProjects.isEmpty() ? unreadProjects : null;
    }
}
