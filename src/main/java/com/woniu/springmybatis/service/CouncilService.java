package com.woniu.springmybatis.service;

import com.woniu.springmybatis.entity.Council;
import com.woniu.springmybatis.mapper.CouncilMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service("CouncilService")
@Transactional(readOnly = true)
public class CouncilService {
	@Resource
	private CouncilMapper councilMapper;

	/**
	 * 得到局级单位列表
	 * @return
	 */
	public List<Council> getCouncilList() {
		List<Council> lists = this.councilMapper.selectAll();
		return lists;
	}
}
