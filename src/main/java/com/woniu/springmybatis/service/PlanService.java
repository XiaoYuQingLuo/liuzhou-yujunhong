package com.woniu.springmybatis.service;

import com.woniu.springmybatis.entity.Plan;
import com.woniu.springmybatis.entity.PlanInformation;
import com.woniu.springmybatis.mapper.PlanMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


@Service
@Transactional(readOnly = true)
public class PlanService {
    @Resource
    private PlanMapper planMapper;
    //新增计划
    @Transactional(readOnly = false)
    public int newPlan(Plan plan){
        // 设置计划状态为待审核
        plan.setPlan_state_id(1);
        // 设置计划时间
        plan.setPlan_time(new Date());
        // 设置审批人
        plan.setPlan_auditor_id(planMapper.findAuditor(plan.getPlan_user_id()));
        planMapper.newPlan(plan);
        return 1;
    }
    // 计划主页展示
    public List<PlanInformation> showPlan(int id){
        List<PlanInformation> plans = planMapper.findPlan(id);
        return plans;
    }
    // 计划删除
    @Transactional(readOnly = false)
    public boolean deletePlan(int id){
        planMapper.deletePlan(id);
        return true;
    }
    // 计划详细信息展示
    public PlanInformation findPlanInformation(int id){
        return this.planMapper.findPlanInformation(id);
    }


}
