package com.woniu.springmybatis.service;

import com.woniu.springmybatis.entity.Position;
import com.woniu.springmybatis.mapper.PositionMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


@Service("PositionService")
@Transactional(readOnly = true)
public class PositionService {
	@Resource
	private PositionMapper positionMapper;

	public List<Position> getPosList() {
		List<Position> lists = this.positionMapper.selectAll();
		return lists;
	}
}
