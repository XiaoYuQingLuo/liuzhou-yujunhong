package com.woniu.springmybatis.service;

import com.woniu.springmybatis.action.UserOperateLog.UserOperate;
import com.woniu.springmybatis.entity.Files;
import com.woniu.springmybatis.mapper.FilesMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("FilesService")
public class FilesService {

    @Resource
    private FilesMapper filesMapper;
    @UserOperate(option ="上传文件" ,module_name = "saveFiles")
    public void saveFiles(Files files){
        this.filesMapper.insert(files);
    }
}
