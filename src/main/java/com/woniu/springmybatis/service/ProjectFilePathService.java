package com.woniu.springmybatis.service;

import com.woniu.springmybatis.entity.Files;
import com.woniu.springmybatis.entity.Project_File_Path;
import com.woniu.springmybatis.mapper.ProjectFilePathMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author YuJunHong
 */
@Service("ProjectFilePathService")
public class ProjectFilePathService {
    @Resource
    private ProjectFilePathMapper projectFilePathMapper;
    public void filePathSave(Files path){
        this.projectFilePathMapper.insert(path);
    }
}
