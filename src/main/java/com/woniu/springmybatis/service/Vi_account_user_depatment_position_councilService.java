package com.woniu.springmybatis.service;

import com.sun.javafx.image.IntPixelGetter;
import com.woniu.springmybatis.entity.Account;
import com.woniu.springmybatis.entity.User;
import com.woniu.springmybatis.entity.Vi_account_user_depatment_position_council;
import com.woniu.springmybatis.mapper.UserMapper;
import com.woniu.springmybatis.mapper.Vi_account_user_depatment_position_councilMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户详细信息的类
 */
@Service("Vi_account_user_depatment_position_councilService")
@Transactional(readOnly = true)
public class Vi_account_user_depatment_position_councilService {
	@Resource
	private Vi_account_user_depatment_position_councilMapper
			vi_account_user_depatment_position_councilMapper;
	@Resource
	private UserMapper userMapper;

	/**
	 * 查询具体用户的所有信息
	 */
	public List<Vi_account_user_depatment_position_council> findAllthing(Integer userID) {
		//按照条件查询

		Example example = new Example(Vi_account_user_depatment_position_council.class);
		example.createCriteria().andCondition("flag=", 0).andCondition("u_id!=", userID);
		List<Vi_account_user_depatment_position_council> vi_account_user_depatment_position_councils
				= this.vi_account_user_depatment_position_councilMapper.selectByExample(example);
		//如果vi_account_user_depatment_position_councils不是空，
		// vi_account_user_depatment_position_councils不是实体，
		// 那么返回vi_account_user_depatment_position_councils集合或者返回空
		return vi_account_user_depatment_position_councils != null &&
				!vi_account_user_depatment_position_councils.isEmpty()
				? vi_account_user_depatment_position_councils : null;
	}

	/**
	 * 查询所有用户的基本信息
	 * 超级管理员可以查看已激活和已注销的所有用户
	 * 一般用户只能查看已激活的用户
	 *
	 * @param account
	 * @return
	 */
	public List<Vi_account_user_depatment_position_council> showAllUsers(Account account) {
		// 判断当前操作用户是否是超级管理员---->account.id==1
		List<Vi_account_user_depatment_position_council> lists;
		Integer id = account.getId();
		if (id == 1) {
			// 超级管理员操作
			lists = this.vi_account_user_depatment_position_councilMapper.selectAll();
		} else {
			// 一般用户操作
			Example example = new Example(Vi_account_user_depatment_position_council.class);
			example.createCriteria().andCondition("flag=", 0);
			lists = this.vi_account_user_depatment_position_councilMapper.selectByExample(example);
		}
		return lists != null && !lists.isEmpty() ? lists : null;
	}

	/**
	 * 根据用户输入的条件搜索用户
	 * 超级管理员可以搜索到已注销的用户，一般用户只能搜索到已激活的用户
	 *
	 * @param searchText
	 * @param account
	 * @return
	 */
	public List<Vi_account_user_depatment_position_council> searchUser(String searchText, Account account) {
		List<Vi_account_user_depatment_position_council> lists;
		// 准备查询条件
		Example example = new Example(Vi_account_user_depatment_position_council.class);
		// 判断当前操作用户身份
		Integer id = account.getId();
		System.out.println("当前操作用户的accoutid是：" + id);
		if (id == 1) {
			// 超级管理员
			// 准备查询条件
			example.createCriteria().orCondition("name=", searchText).orCondition("tel=", searchText).
					orCondition("email=", searchText).orCondition("sex=", searchText).orCondition("d_name=", searchText).
					orCondition("p_name=", searchText).orCondition("council_name=", searchText);
			lists = this.vi_account_user_depatment_position_councilMapper.selectByExample(example);
		} else {
			// 一般用户，只能查询到已激活的用户(flag=0)
			// 准备查询条件
			Example.Criteria c1 = example.createCriteria();
			Example.Criteria c2 = example.createCriteria();
			c1.orEqualTo("name",searchText).orEqualTo("tel",searchText).orEqualTo("email",searchText).
					orEqualTo("sex",searchText).orEqualTo("d_name",searchText).orEqualTo("p_name",searchText).
					orEqualTo("council_name",searchText);
			c2.andEqualTo("flag",0);
			example.and(c2);
			lists = this.vi_account_user_depatment_position_councilMapper.selectByExample(example);
		}
		return lists != null && !lists.isEmpty() ? lists : null;
	}

	/**
	 * 根据操作用户选定的用户id展示用户的全部信息
	 *
	 * @param uid
	 * @return
	 */
	public List<Vi_account_user_depatment_position_council> showUserInfo(String uid) {
		// 格式转换，得到用户id
		Integer u_id = Integer.valueOf(uid);
		// 准备查询条件
		Example example = new Example(Vi_account_user_depatment_position_council.class);
		example.createCriteria().andCondition("u_id=", u_id);
		List<Vi_account_user_depatment_position_council> lists = this.vi_account_user_depatment_position_councilMapper.selectByExample(example);
		return lists;
	}

	/**
	 * 根据操作用户选定的用户id，更新具体用户的状态flag
	 *
	 * @param uid
	 * @return
	 */
	@Transactional(readOnly = false)
	public boolean changeUserStatus(String uid) {
		// 格式转换
		Integer u_id = Integer.valueOf(uid);
		/*User user = new User();
		user.setId(u_id);*/
		// 获取选定用户的状态
		User user = this.userMapper.selectByPrimaryKey(u_id);
		Integer flag = user.getFlag();
		System.out.println(user.getId() + "=========" + flag);
		if (flag == 0) {
			// 用户已激活，需要更改为已注销
			user.setFlag(1);
			this.userMapper.updateByPrimaryKeySelective(user);
		} else {
			// 用户已注销，需要更改为已激活
			user.setFlag(0);
			this.userMapper.updateByPrimaryKeySelective(user);
		}
		return true;
	}
}
