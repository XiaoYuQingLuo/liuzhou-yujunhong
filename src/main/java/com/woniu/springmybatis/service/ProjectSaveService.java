package com.woniu.springmybatis.service;

import com.woniu.springmybatis.entity.Project;
import com.woniu.springmybatis.mapper.ProjectSaveMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service("ProjectSaveService")
@Transactional(readOnly = true)
public class ProjectSaveService {

    @Resource
    private ProjectSaveMapper projectSaveMapper;

    @Transactional(readOnly = false)
    public boolean saveProject(Project project){
        // 判断是否存在影响行 表示插入成功与否

        return this.projectSaveMapper.insert(project)>0;
    }
}
