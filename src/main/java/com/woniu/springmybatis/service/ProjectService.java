package com.woniu.springmybatis.service;

import com.woniu.springmybatis.entity.Business;
import com.woniu.springmybatis.entity.Department;
import com.woniu.springmybatis.entity.Project;
import com.woniu.springmybatis.mapper.BusinessMapper;
import com.woniu.springmybatis.mapper.DepartmentMapper;
import com.woniu.springmybatis.mapper.ProjectMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class ProjectService {

    @Resource
    private ProjectMapper projectMapper;

    @Resource
    private DepartmentMapper departmentMapper;

    @Resource
    private BusinessMapper businessMapper;

    public List<Department> findAllDepartment(){

        List<Department> list = this.departmentMapper.selectAll();
        return list;

    }

    public List<Business> findAllBusiness(){

        List<Business> list = this.businessMapper.selectAll();
        return list;

    }

}
