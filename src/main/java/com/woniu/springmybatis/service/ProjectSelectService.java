package com.woniu.springmybatis.service;

import com.woniu.springmybatis.entity.Account;
import com.woniu.springmybatis.entity.AllProjectInformations;
import com.woniu.springmybatis.entity.Project;
import com.woniu.springmybatis.mapper.ProjectMapper;
import com.woniu.springmybatis.mapper.ProjectSelectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

import static java.lang.Integer.parseInt;

/**
 * @author YuJunHong
 */
@Service("ProjectSelectService")
@Transactional(readOnly = true)
public class ProjectSelectService {
    @Autowired
    private ProjectSelectMapper projectSelectMapper;

    @Resource
    private ProjectMapper projectMapper;

    public List<AllProjectInformations> validateProjectInfor(String content) {

        Example example = new Example(AllProjectInformations.class);
        // 组织查询条件---->使用orcondition
        example.createCriteria().orCondition("pro_name=", content).
                orCondition("pro_state=", content).orCondition("pro_text=",content);
        // 使用通用mapper中的按条件查询
        List<AllProjectInformations> projects = this.projectSelectMapper.selectByExample(example);
        // 三元表达式，返回Account对象
        return projects != null && !projects.isEmpty() ? projects : null;
    }
    public List<AllProjectInformations> validateProjectInformations(String projectId) {

        Example example1 = new Example(AllProjectInformations.class);
        // 组织查询条件---->使用orcondition
        example1.createCriteria().andCondition("id=",projectId);
        // 使用通用mapper中的按条件查询
        List<AllProjectInformations> projects = this.projectSelectMapper.selectByExample(example1);
        // 三元表达式，返回Account对象
        return projects != null && !projects.isEmpty() ? projects : null;
    }
    @Transactional(readOnly = false)
    public void changeCouncilPass(String projectId){
        Integer id = parseInt(projectId);
        this.projectMapper.changeProStatePass(id);
    }
    @Transactional(readOnly = false)
    public void changeCouncilFail(String projectId){
        Integer id = parseInt(projectId);
        this.projectMapper.changeProStateFail(id);
    }

}
