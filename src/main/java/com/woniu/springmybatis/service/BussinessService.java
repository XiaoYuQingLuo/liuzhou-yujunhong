package com.woniu.springmybatis.service;
import com.woniu.springmybatis.entity.Vi_bussiness_state_user;
import com.woniu.springmybatis.mapper.Vi_Bussiness_s_u_Mapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author XieZhuying
 */
@Service("BussniessService")
@Transactional(readOnly = true)
public class BussinessService {
    //注入
    @Resource
    private Vi_Bussiness_s_u_Mapper vi_bussiness_s_u_mapper;

    /**
     * 用于展示招商引资库里所有的信息的Service
     * @return
     */
    public List<Vi_bussiness_state_user> bussinessSelect(){
        //查询全部
        List<Vi_bussiness_state_user> bussiness = vi_bussiness_s_u_mapper.selectAll();
        return bussiness;
    }

    /**
     * 用于查询招商引资库里的所有信息Service
     */
    public List<Vi_bussiness_state_user>SearchByCondition(String content){
        Example example = new Example(Vi_bussiness_state_user.class);
        //组织查询条件
        System.out.println(content+"====>在Service里获得的Content");
        example.createCriteria().andCondition("address=",content);
        //使用通用Mapper里的条件查询
        List<Vi_bussiness_state_user> businesses = vi_bussiness_s_u_mapper.selectByExample(example);
        //三元表达式
        return businesses != null && !businesses.isEmpty() ? businesses : null;
    }

}
