package com.woniu.springmybatis.service;

import com.woniu.springmybatis.entity.*;
import com.woniu.springmybatis.mapper.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

@Service("departmentService")
@Transactional(readOnly = true)
public class DepartmentService {
	@Resource
	private DepartmentMapper departmentMapper;
	@Resource
	private Vi_user_dep_councilMapper vi_user_dep_councilMapper;
	@Resource
	private Vi_dep_userMapper vi_dep_userMapper;
	@Resource
	private Vi_account_user_depatment_position_councilMapper vi_account_user_depatment_position_councilMapper;
	@Resource
	private CouncilMapper councilMapper;
	@Resource
	private UserMapper userMapper;

	/**
	 * 搜索所有的部门信息（部门名，所属单位名，部门领导）
	 *
	 * @return
	 */
	public List<Vi_user_dep_council> showAllDep() {
		// 从数据库的vi_user_dep_council视图中得到所有数据
		List<Vi_user_dep_council> deps = this.vi_user_dep_councilMapper.selectAll();
		return deps != null && !deps.isEmpty() ? deps : null;
	}

	/**
	 * 根据用户输入的数据查询视图vi_user_dep_council得到具体的部门信息
	 *
	 * @param content
	 * @return
	 */
	public List<Vi_user_dep_council> searchDep(String content) {
		// 组织查询条件
		Example example = new Example(Vi_user_dep_council.class);
		example.createCriteria().orCondition("d_name=", content).
				orCondition("council_name=", content).orCondition("leader=", content);
		// 使用通用mapper中的按条件查询
		List<Vi_user_dep_council> deps = this.vi_user_dep_councilMapper.selectByExample(example);
		/*
		三元表达式，返回Vi_user_dep_council对象
		如果没有查询结果则返回null
		*/
		return deps != null && !deps.isEmpty() ? deps : null;
	}

	/**
	 * 得到具体部门下属员工的基本信息
	 *
	 * @param dep_id
	 * @return
	 */
	public List<Vi_account_user_depatment_position_council> getDepUser(String dep_id) {
		Example example = new Example(Vi_dep_user.class);
		example.createCriteria().andCondition("d_id=", dep_id).andCondition("p_id=", 2);
		/*List<Vi_dep_user> lists = this.vi_dep_userMapper.selectByExample(example);*/
		List<Vi_account_user_depatment_position_council> lists = this.vi_account_user_depatment_position_councilMapper.selectByExample(example);
		return lists;
	}

	/**
	 * 得到具体部门的部门名称、部门领导和所属局、部门简介
	 *
	 * @param dep_id
	 * @return
	 */
	public Vi_user_dep_council getDep(String dep_id) {
		Example example = new Example(Vi_user_dep_council.class);
		example.createCriteria().andCondition("d_id=", dep_id);
		List<Vi_user_dep_council> vi_user_dep_councils = this.vi_user_dep_councilMapper.selectByExample(example);
		return vi_user_dep_councils.get(0);
	}

	/**
	 * 修改具体部门的信息：部门名称和部门简介
	 *
	 * @param d_id
	 * @return
	 */
	@Transactional(readOnly = false)
	public boolean updateDepInfo(Department dep) {
		// 受影响的行
		int i = this.departmentMapper.updateByPrimaryKeySelective(dep);
		if (i == 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 删除具体部门
	 *
	 * @param dep_id
	 * @return
	 */
	@Transactional(readOnly = false)
	public int deleteDep(String dep_id) {
		// 数据类型转换，得到部门id
		Integer id = Integer.valueOf(dep_id);
		Department dep = new Department();
		dep.setId(id);
		int i = this.departmentMapper.deleteByPrimaryKey(dep);
		return i;
	}

	/**
	 * 新增部门-得到局级单位list
	 *
	 * @return
	 */
	public List<Council> getCouncilList() {
		List<Council> councils = this.councilMapper.selectAll();
		for (int i = 0; i < councils.size(); i++) {
			System.out.println(councils.get(i));
		}
		return councils;
	}

	/**
	 * 新增部门-得到基层员工的List
	 *
	 * @return
	 */
	public List<User> getStaffList() {
		// 组织查询条件
		Example example = new Example(User.class);
		example.createCriteria().andCondition("p_id =", 2);
		List<User> users = this.userMapper.selectByExample(example);
		for (int i = 0; i < users.size(); i++) {
			System.out.println(users.get(i));
		}
		return users;
	}

	/**
	 * 新增部门
	 *
	 * @param dep
	 * @return
	 */
	@Transactional(readOnly = false)
	public int addNewDep(Department dep) {
		int i = this.departmentMapper.insertSelective(dep);
		System.out.println("新增部门，受影响的行：" + i);
		int newDepId = this.departmentMapper.getMaxId();
		return newDepId;
	}

	/**
	 * 根据局级单位的id查询下属部门列表
	 * @param councilId
	 * @return
	 */
	public List<Department> getDepList(String councilId) {
		// 格式转换
		Integer council_id = Integer.valueOf(councilId);
		Example example = new Example(Department.class);
		example.createCriteria().andEqualTo("council_id",council_id);
		List<Department> lists = this.departmentMapper.selectByExample(example);
		return lists;
	}
}
