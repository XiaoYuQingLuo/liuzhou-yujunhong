package com.woniu.springmybatis.service;

import com.woniu.springmybatis.entity.User_operatelog;
import com.woniu.springmybatis.mapper.User_operateLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户操作日志的service类
 */
@Service("User_operatelogService")
public class User_operatelogService {

    @Autowired
    private User_operateLogMapper user_operateLogMapper;

    /**
     * 对操作信息进行插入
     * @param user_operatelog
     * @return
     */
    public int insertLog(User_operatelog user_operatelog){

        int i = this.user_operateLogMapper.insertSelective(user_operatelog);
        return i;
    }
}
