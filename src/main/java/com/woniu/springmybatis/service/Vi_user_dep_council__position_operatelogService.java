package com.woniu.springmybatis.service;

import com.woniu.springmybatis.entity.Vi_user_dep_council__position_operatelog;
import com.woniu.springmybatis.mapper.Vi_user_dep_council__position_operatelogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户操作日志service
 */
@Service("Vi_user_dep_council__position_operatelogService")
public class Vi_user_dep_council__position_operatelogService {
    @Resource
    private Vi_user_dep_council__position_operatelogMapper vi_user_dep_council__position_operatelogMapper;

    /**
     * 查询全部操作日志
     * @return
     */
    public List<Vi_user_dep_council__position_operatelog> selcetAllInfo(){
        Example example = new Example(Vi_user_dep_council__position_operatelog.class);
        example.setOrderByClause("time DESC");
        example.createCriteria().getAllCriteria();
        return this.vi_user_dep_council__position_operatelogMapper.selectByExample(example);
    }
}
