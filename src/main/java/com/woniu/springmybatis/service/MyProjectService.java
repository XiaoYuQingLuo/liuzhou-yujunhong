package com.woniu.springmybatis.service;

import com.woniu.springmybatis.entity.AllProjectInformations;
import com.woniu.springmybatis.entity.Project;
import com.woniu.springmybatis.entity.User;
import com.woniu.springmybatis.mapper.MyProjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.servlet.http.HttpSession;
import java.util.List;

@Service("MyProjectService")
@Transactional(readOnly = true)
public class MyProjectService {
    @Autowired
    private MyProjectMapper myProjectMapper;

    public List<Project> validateMyProject(Integer id) {
        Example example = new Example(Project.class);
        // 组织查询条件---->使用andcondition
        example.createCriteria().andCondition("user_id=", id);

        // 使用通用mapper中的按条件查询
       List<Project> myProjects=this.myProjectMapper.selectByExample(example);
        // 三元表达式，返回Account对象
        return myProjects != null && !myProjects.isEmpty() ? myProjects : null;
    }
}
