package com.woniu.springmybatis.service;

import com.woniu.springmybatis.entity.Account;
import com.woniu.springmybatis.entity.User;
import com.woniu.springmybatis.entity.Vi_council_department;
import com.woniu.springmybatis.mapper.AccountMapper;
import com.woniu.springmybatis.mapper.UserMapper;
import com.woniu.springmybatis.mapper.Vi_council_departmentMapper;
import com.woniu.springmybatis.tools.AccountUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 用户个人资料查询
 */
@Service("userService")
@Transactional(readOnly = true)
public class UserService {
	@Resource
	private UserMapper userMapper;
	@Resource
	private Vi_council_departmentMapper vi_council_departmentMapper;
	@Resource
	private AccountMapper accountMapper;

	/**
	 * 通过账户ID查找用户的信息
	 *
	 * @param id
	 * @return
	 */
	public User findUserByAccountID(Integer id) {
		//按照条件查询
		Example example = new Example(User.class);
		// 通过账户id，查找用户信息
		example.createCriteria().andCondition("a_id=", id).andCondition("flag=", 0);
		List<User> users = this.userMapper.selectByExample(example);
		return users != null && !users.isEmpty() ? users.get(0) : null;
	}

	/**
	 * 通过用户ID查找用户信息
	 */
	public User findUserByUserID(Integer id) {
		//按照条件查询
		Example example = new Example(User.class);
		// 通过用户id，查找用户信息
		example.createCriteria().andCondition("id=", id).andCondition("flag=", 0);
		List<User> users = this.userMapper.selectByExample(example);
		return users != null && !users.isEmpty() ? users.get(0) : null;
	}

	/**
	 * 通过用户ID查找用户领导
	 */
	public User findLeader(Integer id) {
		return this.userMapper.selectLeader(id).get(0);
	}

	/**
	 * 设置某用户为某局下属某部门的领导
	 *
	 * @param c_id
	 * @param newDepId
	 * @param leader_id
	 * @return
	 */
	@Transactional(readOnly = false)
	public int setDepLeader(Integer c_id, int newDepId, Integer leader_id) {
		User user = new User();
		user.setId(leader_id);
		user.setC_id(c_id);
		user.setD_id(newDepId);
		user.setP_id(1);
		int row = this.userMapper.updateByPrimaryKeySelective(user);
		return row;
	}

	/**
	 * 得到局级单位-部门对应信息表
	 *
	 * @return
	 */
	public List<Vi_council_department> getCouncilDepList() {
		List<Vi_council_department> lists = this.vi_council_departmentMapper.selectAll();
		return lists != null && !lists.isEmpty() ? lists : null;
	}

	/**
	 * 根据用户输入、选择的数据新增用户
	 *
	 * @param
	 * @param map
	 * @return
	 */
	@Transactional(readOnly = false)
	public int addNewUser(Map<String, String> map) {
		// 验证
		boolean isAllGiven = AccountUtil.isAllGiven(map);
		if(!isAllGiven){
			// 所需信息有欠缺
			return 0;
		}
		// 拆包参数
		// 登录名
		String loginAccount = map.get("loginAccount");
		// 登录密码
		String loginPass = map.get("loginPass");
		// 确认登录密码
		String confirmLoginPass = map.get("confirmLoginPass");
		// 姓名
		String name = map.get("name");
		// 所属局级单位id
		Integer councilId = Integer.valueOf(map.get("council"));
		// 所属部门id
		Integer departmentId = Integer.valueOf(map.get("department"));
		// 用户职位id
		/*Integer positionId = Integer.valueOf(map.get("position"));*/
		/*
		向account表中新增登录信息
		要求：
		1-登录名不能重复
		2-登录名5~30位
		3-登录密码5~32位
		4-两次输入的登录密码必须相同
		5-密码必须经过MD5加密写入数据库
		 */
		// 1-判断登录名是否重复
		boolean isLoginAccountRepeat = isLoginAccountRepeat(loginAccount);
		if (isLoginAccountRepeat) {
			// 登录名已经重复，返回1
			return 1;
		}
		// 2-判断登录名长度
		boolean isLoginAccountLengthLegal =AccountUtil.isLoginAccountLengthLegal(loginAccount);
		if (!isLoginAccountLengthLegal) {
			// 登录名长度不合法，返回2
			return 2;
		}
		// 3-判断登录密码长度
		boolean isLoginPassLengthLegal = AccountUtil.isLoginPassLengthLegal(loginPass);
		if (!isLoginPassLengthLegal) {
			// 登录密码长度不合法
			return 3;
		}
		// 4-判断两次登录密码是否一致
		boolean isPassEqualsConfirmPass = AccountUtil.isPassEqualsConfirmPass(loginPass,confirmLoginPass);
		if (!isPassEqualsConfirmPass) {
			// 两次登录密码不一致
			return 4;
		}
		// 只有当四者都符合要求时，才对密码进行加密，然后写入数据库
		/*if(isLoginAccountRepeat || !isLoginAccountLengthLegal || !isLoginPassLengthLegal || !isPassEqualsConfirmPass){
			// 登录名重复或者登录名长度不合法或者登录密码长度不合法或者两次密码不一致
			return false;
		}*/
		// 对密码进行加密处理
		String loginPassMD5 = DigestUtils.md5DigestAsHex(loginPass.getBytes());
		// 写入数据库表account
		Account account = new Account();
		account.setUsername(loginAccount);
		account.setPassword(loginPassMD5);
		int insertIntoAccount = this.accountMapper.insertSelective(account);
		if (insertIntoAccount != 1) {
			// 写入account表失败
			return 5;
		}
		// 在account表中插入成功之后取得表中最大的主键id
		Integer newAccountId = this.accountMapper.getMaxId();
		// 向user表中插入数据
		User user = new User();
		user.setA_id(newAccountId);
		user.setC_id(councilId);
		user.setD_id(departmentId);
		// 默认新增用户的职位都是职员，职位id是2
		user.setP_id(2);
		user.setName(name);
		int insertIntoUser = this.userMapper.insertSelective(user);
		if (insertIntoUser != 1) {
			// 写入user表失败
			return 6;
		}
		// 写入表account和user都成功，完成新增用户
		return 7;
	}

	/**
	 * 判断在数据库表account中是否已经存在登录用户名
	 * 不能加入工具类，残念!
	 * @param loginAccount
	 * @return
	 */
	public boolean isLoginAccountRepeat(String loginAccount) {
		Example example = new Example(Account.class);
		example.createCriteria().andEqualTo("username", loginAccount);
		int i = this.accountMapper.selectCountByExample(example);
		if (i == 0) {
			// 查询account表，登录用户名不存在
			return false;
		}
		return true;
	}

}
