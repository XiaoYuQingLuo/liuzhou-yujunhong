package com.woniu.springmybatis.service;


import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;


import java.io.File;
import java.util.UUID;

@Service("UploadService")
@Transactional(readOnly = false)
public class UploadService {


    /**
     * 文件上传
     * @param photo
     */
        public void fileUpload(MultipartFile photo) {
            // 判断文件上传
            if (!photo.isEmpty()) {
                try {
                    // 获取该文件的原始名
                    String originalFilename = photo.getOriginalFilename();
                    // uuid+文件名
                    String fileName = UUID.randomUUID() + originalFilename.substring(originalFilename.lastIndexOf("."));
                    //文件夹
                    System.out.println("fileName:"+fileName);
                    File file = new File("／D:/java/images", fileName);
                    photo.transferTo(file);
                } catch (Exception e) {

                }
            }

        }

    }

