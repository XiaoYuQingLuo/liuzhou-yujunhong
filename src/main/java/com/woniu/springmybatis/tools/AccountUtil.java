package com.woniu.springmybatis.tools;

import java.util.Map;

/**
 * 新增用户，将信息添加到account表的工具类
 */
public class AccountUtil {
	/**
	 * 验证登录名长度是否合法
	 *
	 * @param loginAccount
	 * @return
	 */
	public static boolean isLoginAccountLengthLegal(String loginAccount) {
		int length = loginAccount.length();
		if (length >= 5 && length <= 30) {
			// 登录名长度在[5,30]，登录名长度合法
			return true;
		}
		return false;
	}

	/**
	 * 验证登录密码长度是否合法
	 *
	 * @param loginPass
	 * @return
	 */
	public static boolean isLoginPassLengthLegal(String loginPass) {
		int length = loginPass.length();
		if (length >= 5 && length <= 32) {
			// 登录密码长度在[5,32]，登录密码长度合法
			return true;
		}
		return false;
	}

	/**
	 * 验证两次输入的登录密码是否一致
	 *
	 * @param loginPass
	 * @param confirmLoginPass
	 * @return
	 */
	public static boolean isPassEqualsConfirmPass(String loginPass, String confirmLoginPass) {
		if (loginPass.equals(confirmLoginPass)) {
			// 两次密码一致
			return true;
		}
		return false;
	}

	/**
	 * 验证在新增用户过程中必要信息是否都已经给出
	 *
	 * @param map
	 * @return
	 */
	public static boolean isAllGiven(Map<String, String> map) {
		// 登录名
		String loginAccount = map.get("loginAccount");
		// 登录密码
		String loginPass = map.get("loginPass");
		// 确认登录密码
		String confirmLoginPass = map.get("confirmLoginPass");
		// 姓名
		String name = map.get("name");
		// 所属局级单位id
		String council = map.get("council");
		// 所属部门id
		String department = map.get("department");
		if ((null != loginAccount && !"".equals(loginAccount)) && (null != loginPass && !"".equals(loginPass))
				&& (null != confirmLoginPass && !"".equals(confirmLoginPass))
				&& (null != name && !"".equals(name))
				&& (null != council && !"".equals(council))
				&& (null != department && !"".equals(department))) {
			// 所需信息都已经给到
			return true;
		}
		return false;
	}
}
