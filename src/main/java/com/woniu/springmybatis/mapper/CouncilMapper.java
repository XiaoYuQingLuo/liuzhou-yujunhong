package com.woniu.springmybatis.mapper;

import com.woniu.springmybatis.entity.Council;
import tk.mybatis.mapper.common.Mapper;

public interface CouncilMapper extends Mapper<Council> {
}
