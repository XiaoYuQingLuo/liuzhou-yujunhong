package com.woniu.springmybatis.mapper;

import com.woniu.springmybatis.entity.Project;
import tk.mybatis.mapper.common.Mapper;

/**
 * 新建项目Mapper
 */
public interface ProjectSaveMapper extends Mapper<Project> {
}
