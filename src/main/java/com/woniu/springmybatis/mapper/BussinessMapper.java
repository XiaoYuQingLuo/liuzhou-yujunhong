package com.woniu.springmybatis.mapper;

import com.woniu.springmybatis.entity.Business;
import tk.mybatis.mapper.common.Mapper;

/**
 * 招商引资库Mapper
 * @author XieZhuYing
 */
public interface BussinessMapper extends Mapper<Business>{


}
