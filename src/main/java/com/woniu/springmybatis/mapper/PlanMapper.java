package com.woniu.springmybatis.mapper;

import com.woniu.springmybatis.entity.Plan;
import com.woniu.springmybatis.entity.PlanInformation;
import com.woniu.springmybatis.entity.User;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 计划信息
 */
@org.apache.ibatis.annotations.Mapper
public interface PlanMapper{
    public void newPlan(Plan plan);
    // 根据用户id查询直接审批人id
    public int findAuditor(int id);
    // 根据用户id查询所有属于自己的计划(发起人为自己或者审核人是自己)
    public List<PlanInformation> findPlan(int id);
    // 根据id删除计划
    public void deletePlan(int id);
    // 根据计划id查询计划详细信息
    public PlanInformation findPlanInformation(int id);
}
