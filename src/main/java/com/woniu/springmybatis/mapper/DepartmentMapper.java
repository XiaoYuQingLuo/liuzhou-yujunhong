package com.woniu.springmybatis.mapper;

import com.woniu.springmybatis.entity.Department;
import tk.mybatis.mapper.common.Mapper;

public interface DepartmentMapper extends Mapper<Department> {

	public int getMaxId();
}
