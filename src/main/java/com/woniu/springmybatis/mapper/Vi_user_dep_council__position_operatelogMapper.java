package com.woniu.springmybatis.mapper;

import com.woniu.springmybatis.entity.Vi_user_dep_council__position_operatelog;
import tk.mybatis.mapper.common.Mapper;

/**
 * 用户操作日志
 */
@org.apache.ibatis.annotations.Mapper
public interface Vi_user_dep_council__position_operatelogMapper extends Mapper<Vi_user_dep_council__position_operatelog> {
}
