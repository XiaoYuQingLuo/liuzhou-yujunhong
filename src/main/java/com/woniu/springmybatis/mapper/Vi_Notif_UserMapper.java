package com.woniu.springmybatis.mapper;

import com.woniu.springmybatis.entity.Vi_Notif_User;
import tk.mybatis.mapper.common.Mapper;
/**
 * 个人信息查询页面
 */
@org.apache.ibatis.annotations.Mapper
public interface Vi_Notif_UserMapper extends Mapper<Vi_Notif_User>{
}
