package com.woniu.springmybatis.mapper;

import com.woniu.springmybatis.entity.User;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 登录用户的个人信息
 */
@org.apache.ibatis.annotations.Mapper
public interface UserMapper extends Mapper<User> {
    public List<User> selectLeader(int id);
}
