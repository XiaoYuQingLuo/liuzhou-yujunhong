package com.woniu.springmybatis.mapper;

import com.woniu.springmybatis.entity.AllProjectInformations;
import tk.mybatis.mapper.common.Mapper;

public interface ProjectMaxIdMapper extends Mapper<AllProjectInformations> {

	public int getMaxId();
}
