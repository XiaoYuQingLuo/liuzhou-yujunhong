package com.woniu.springmybatis.mapper;

import com.woniu.springmybatis.entity.Account;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;


/**
 * 账号信息
 * @author Taylor
 */
@org.apache.ibatis.annotations.Mapper
public interface AccountMapper extends Mapper<Account>{
//    Account findUserById(int id);
	Integer getMaxId();
}
