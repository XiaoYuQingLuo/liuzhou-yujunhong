package com.woniu.springmybatis.mapper;

import com.woniu.springmybatis.entity.AllProjectInformations;
import com.woniu.springmybatis.entity.Files;
import tk.mybatis.mapper.common.Mapper;

public interface MissionMaxIdMapper extends Mapper<Files> {

	public int getMaxId();
}
