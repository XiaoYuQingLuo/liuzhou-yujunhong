package com.woniu.springmybatis.mapper;

import com.woniu.springmybatis.entity.Mission;
import tk.mybatis.mapper.common.Mapper;

public interface CreateMissionMapper extends Mapper<Mission>{
}
