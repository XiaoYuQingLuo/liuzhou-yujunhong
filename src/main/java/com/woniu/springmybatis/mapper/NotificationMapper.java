package com.woniu.springmybatis.mapper;

import com.woniu.springmybatis.entity.Notification;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/**
 * 个人通知的查询类
 */
@org.apache.ibatis.annotations.Mapper
public interface NotificationMapper extends Mapper<Notification> {

    int selectCount();
}
