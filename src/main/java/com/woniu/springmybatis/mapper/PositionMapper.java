package com.woniu.springmybatis.mapper;

import com.woniu.springmybatis.entity.Position;
import tk.mybatis.mapper.common.Mapper;

public interface PositionMapper extends Mapper<Position> {
}
