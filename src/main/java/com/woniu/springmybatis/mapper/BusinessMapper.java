package com.woniu.springmybatis.mapper;

import com.woniu.springmybatis.entity.Business;
import tk.mybatis.mapper.common.Mapper;

public interface BusinessMapper extends Mapper<Business> {
}
