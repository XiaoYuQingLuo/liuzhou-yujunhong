package com.woniu.springmybatis.mapper;

import com.woniu.springmybatis.entity.Files;
import com.woniu.springmybatis.entity.Project_File_Path;
import tk.mybatis.mapper.common.Mapper;

public interface ProjectFilePathMapper extends Mapper<Files> {
}
