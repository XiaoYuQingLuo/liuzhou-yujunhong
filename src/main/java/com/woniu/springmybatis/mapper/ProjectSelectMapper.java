package com.woniu.springmybatis.mapper;

import com.woniu.springmybatis.entity.AllProjectInformations;
import com.woniu.springmybatis.entity.Project;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author YuJunHong
 */
public interface ProjectSelectMapper extends Mapper<AllProjectInformations>{

}
