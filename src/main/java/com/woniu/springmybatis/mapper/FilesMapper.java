package com.woniu.springmybatis.mapper;

import com.woniu.springmybatis.entity.Files;
import tk.mybatis.mapper.common.Mapper;

public interface FilesMapper extends Mapper<Files> {

    void insertFiles(Files files);
}
