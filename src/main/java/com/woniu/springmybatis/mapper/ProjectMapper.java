package com.woniu.springmybatis.mapper;

import com.woniu.springmybatis.entity.Project;
import tk.mybatis.mapper.common.Mapper;

public interface ProjectMapper extends Mapper<Project> {
    public void changeProStatePass(int id);
    public void changeProStateFail(int id);
}
