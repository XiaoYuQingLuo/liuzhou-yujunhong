package com.woniu.springmybatis.mapper;

import com.woniu.springmybatis.entity.Project;
import tk.mybatis.mapper.common.Mapper;

public interface UnreadProjectMapper extends Mapper<Project> {
}
