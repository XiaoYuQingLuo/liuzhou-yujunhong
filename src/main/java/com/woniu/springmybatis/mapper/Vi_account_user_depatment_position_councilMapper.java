package com.woniu.springmybatis.mapper;


import com.woniu.springmybatis.entity.Vi_account_user_depatment_position_council;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface Vi_account_user_depatment_position_councilMapper
        extends Mapper<Vi_account_user_depatment_position_council> {
}
