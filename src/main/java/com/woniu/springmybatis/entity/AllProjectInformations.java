package com.woniu.springmybatis.entity;

public class AllProjectInformations {
            private Integer id;
            private String name;
            private String pro_state;
            private String pro_type;
            private String pro_level;
            private String start_time;
            private String finish_time;
            private String pro_name;
            private Integer p_id;
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPro_name() {
        return pro_name;
    }

    public void setPro_name(String pro_name) {
        this.pro_name = pro_name;
    }

    private String pro_text;
            private String funds;
            private String ground;
            private String build_time;
            private String update_time;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPro_state() {
        return pro_state;
    }

    public void setPro_state(String pro_state) {
        this.pro_state = pro_state;
    }

    public String getPro_type() {
        return pro_type;
    }

    public void setPro_type(String pro_type) {
        this.pro_type = pro_type;
    }

    public String getPro_level() {
        return pro_level;
    }

    public void setPro_level(String pro_level) {
        this.pro_level = pro_level;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getFinish_time() {
        return finish_time;
    }

    public void setFinish_time(String finish_time) {
        this.finish_time = finish_time;
    }

    public String getPro_text() {
        return pro_text;
    }

    public void setPro_text(String pro_text) {
        this.pro_text = pro_text;
    }

    public String getFunds() {
        return funds;
    }

    public void setFunds(String funds) {
        this.funds = funds;
    }

    public String getGround() {
        return ground;
    }

    public void setGround(String ground) {
        this.ground = ground;
    }

    public String getBuild_time() {
        return build_time;
    }

    public void setBuild_time(String build_time) {
        this.build_time = build_time;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public Integer getP_id() {
        return p_id;
    }

    public void setP_id(Integer p_id) {
        this.p_id = p_id;
    }
}
