package com.woniu.springmybatis.entity;

import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 这个是个人通知的类
 */
@Table(name = "notification")
public class Notification {
    @Id
    private Integer id;
    private Integer send_user_id;//发送人
    private Integer recieve_user_id;//接收人
    private Date send_time;//发送时间
    private String notif_title;//主题
    private String notif_text;//内容
    private Integer notif_state;//是否已读
    private Integer flag;

    public void setId(Integer id) {
        this.id = id;
    }

    public void setSend_user_id(Integer send_user_id) {
        this.send_user_id = send_user_id;
    }

    public void setRecieve_user_id(Integer recieve_user_id) {
        this.recieve_user_id = recieve_user_id;
    }

    public void setSend_time(Date send_time) {
        this.send_time = send_time;
    }

    public void setNotif_title(String notif_title) {
        this.notif_title = notif_title;
    }

    public void setNotif_text(String notif_text) {
        this.notif_text = notif_text;
    }

    public void setNotif_state(Integer notif_state) {
        this.notif_state = notif_state;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public Integer getId() {

        return id;
    }

    public Integer getSend_user_id() {
        return send_user_id;
    }

    public Integer getRecieve_user_id() {
        return recieve_user_id;
    }

    public Date getSend_time() {
        return send_time;
    }

    public String getNotif_title() {
        return notif_title;
    }

    public String getNotif_text() {
        return notif_text;
    }

    public Integer getNotif_state() {
        return notif_state;
    }

    public Integer getFlag() {
        return flag;
    }


}
