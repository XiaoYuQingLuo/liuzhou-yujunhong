package com.woniu.springmybatis.entity;

import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 招商引资库
 */
@Table(name="business")
public class Business {
    @Id
    private Integer id;
    private String com_name;//企业名称
    private String industry;//企业类型
    private Date time;//入库时间
    private String user_id;//创建人
    private String address;//企业地址
    private Integer state;//是否已读

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCom_name() {
        return com_name;
    }

    public void setCom_name(String com_name) {
        this.com_name = com_name;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    private Integer flag;//标志位

}
