package com.woniu.springmybatis.entity;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 对应部门信息展示的视图
 */
@Table(name = "vi_user_dep_council")
public class Vi_user_dep_council {
	// 部门id
	private Integer d_id;
	// 部门名称
	private String d_name;
	// 所属局名称
	private String council_name;
	// 部门领导
	private String leader;
	// 部门简介
	private String d_text;

	public Integer getD_id() {
		return d_id;
	}

	public void setD_id(Integer d_id) {
		this.d_id = d_id;
	}

	public String getD_name() {
		return d_name;
	}

	public void setD_name(String d_name) {
		this.d_name = d_name;
	}

	public String getCouncil_name() {
		return council_name;
	}

	public void setCouncil_name(String council_name) {
		this.council_name = council_name;
	}

	public String getLeader() {
		return leader;
	}

	public void setLeader(String leader) {
		this.leader = leader;
	}

	public String getD_text() {
		return d_text;
	}

	public void setD_text(String d_text) {
		this.d_text = d_text;
	}

	@Override
	public String toString() {
		return "Vi_user_dep_council{" +
				"d_id=" + d_id +
				", d_name='" + d_name + '\'' +
				", council_name='" + council_name + '\'' +
				", leader='" + leader + '\'' +
				", d_text='" + d_text + '\'' +
				'}';
	}
}
