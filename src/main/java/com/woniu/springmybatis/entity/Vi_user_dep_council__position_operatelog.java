package com.woniu.springmybatis.entity;

import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 用户操作日志
 */
@Table(name = "vi_user_dep_council__position_operatelog")
public class Vi_user_dep_council__position_operatelog {
        @Id
          private Integer id          ;
          private Integer user_id     ;
          private String module_name  ;/**方法名称*/
          private String operate_desc;/**描述*/
          private String ip_address  ;
          private String time      ;
          private Integer d_id        ;
          private String d_name      ;
          private Integer p_id        ;
          private String p_name      ;
          private Integer c_id        ;
          private String council_name;
          private String user_name   ;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getUser_id() {
    return user_id;
  }

  public void setUser_id(Integer user_id) {
    this.user_id = user_id;
  }

  public String getModule_name() {
    return module_name;
  }

  public void setModule_name(String module_name) {
    this.module_name = module_name;
  }

  public String getOperate_desc() {
    return operate_desc;
  }

  public void setOperate_desc(String operate_desc) {
    this.operate_desc = operate_desc;
  }

  public String getIp_address() {
    return ip_address;
  }

  public void setIp_address(String ip_address) {
    this.ip_address = ip_address;
  }

  public String getTime() {
    return time;
  }

  public void setTime(String time) {
    this.time = time;
  }

  public Integer getD_id() {
    return d_id;
  }

  public void setD_id(Integer d_id) {
    this.d_id = d_id;
  }

  public String getD_name() {
    return d_name;
  }

  public void setD_name(String d_name) {
    this.d_name = d_name;
  }

  public Integer getP_id() {
    return p_id;
  }

  public void setP_id(Integer p_id) {
    this.p_id = p_id;
  }

  public String getP_name() {
    return p_name;
  }

  public void setP_name(String p_name) {
    this.p_name = p_name;
  }

  public Integer getC_id() {
    return c_id;
  }

  public void setC_id(Integer c_id) {
    this.c_id = c_id;
  }

  public String getCouncil_name() {
    return council_name;
  }

  public void setCouncil_name(String council_name) {
    this.council_name = council_name;
  }

  public String getUser_name() {
    return user_name;
  }

  public void setUser_name(String user_name) {
    this.user_name = user_name;
  }
}
