package com.woniu.springmybatis.entity;

import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "vi_council_department")
public class Vi_council_department {
	@Id
	private Integer id;
	private String d_name;
	private String d_text;
	private String council_name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getD_name() {
		return d_name;
	}

	public void setD_name(String d_name) {
		this.d_name = d_name;
	}

	public String getD_text() {
		return d_text;
	}

	public void setD_text(String d_text) {
		this.d_text = d_text;
	}

	public String getCouncil_name() {
		return council_name;
	}

	public void setCouncil_name(String council_name) {
		this.council_name = council_name;
	}

	@Override
	public String toString() {
		return "Vi_council_department{" +
				"id=" + id +
				", d_name='" + d_name + '\'' +
				", d_text='" + d_text + '\'' +
				", council_name='" + council_name + '\'' +
				'}';
	}
}
