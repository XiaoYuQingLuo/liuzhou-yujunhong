package com.woniu.springmybatis.entity;

import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

/**
 * 局级单位对应实体类
 */
@Table(name = "council")
public class Council {
    @Id
    private Integer id;
    // 局级单位名
    private String council_name;
    // 局和部门是一对多的关系
    private List<Department> departments;
    private Integer flag;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCouncil_name() {
        return council_name;
    }

    public void setCouncil_name(String council_name) {
        this.council_name = council_name;
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "Council{" +
                "id=" + id +
                ", council_name='" + council_name + '\'' +
                ", departments=" + departments +
                ", flag=" + flag +
                '}';
    }
}
