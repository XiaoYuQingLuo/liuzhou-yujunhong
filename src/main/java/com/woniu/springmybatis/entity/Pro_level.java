package com.woniu.springmybatis.entity;

public class Pro_level {

    private Integer id;
    private String pro_level;
    private Integer flag;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPro_level() {
        return pro_level;
    }

    public void setPro_level(String pro_level) {
        this.pro_level = pro_level;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }
}
