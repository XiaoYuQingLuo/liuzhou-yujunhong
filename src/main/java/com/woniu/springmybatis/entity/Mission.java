package com.woniu.springmybatis.entity;

public class Mission {
    private Integer id;
    private Integer project_id;
    private String mission_name;
    private String mission_text;
    private Integer dep_id;
    private String start_time;
    private String plan_time;
    private Integer finish_state;
    private Integer flag;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProject_id() {
        return project_id;
    }

    public void setProject_id(Integer project_id) {
        this.project_id = project_id;
    }

    public String getMission_name() {
        return mission_name;
    }

    public void setMission_name(String mission_name) {
        this.mission_name = mission_name;
    }

    public String getMission_text() {
        return mission_text;
    }

    public void setMission_text(String mission_text) {
        this.mission_text = mission_text;
    }

    public Integer getDep_id() {
        return dep_id;
    }

    public void setDep_id(Integer dep_id) {
        this.dep_id = dep_id;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getPlan_time() {
        return plan_time;
    }

    public void setPlan_time(String plan_time) {
        this.plan_time = plan_time;
    }

    public Integer getFinish_state() {
        return finish_state;
    }

    public void setFinish_state(Integer finish_state) {
        this.finish_state = finish_state;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }
}
