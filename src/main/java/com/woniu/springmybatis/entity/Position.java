package com.woniu.springmybatis.entity;

import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "position")
public class Position {
	@Id
	private Integer id;
	private String p_name;
	private String p_text;
	private Integer flag;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getP_name() {
		return p_name;
	}

	public void setP_name(String p_name) {
		this.p_name = p_name;
	}

	public String getP_text() {
		return p_text;
	}

	public void setP_text(String p_text) {
		this.p_text = p_text;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	@Override
	public String toString() {
		return "Position{" +
				"id=" + id +
				", p_name='" + p_name + '\'' +
				", p_text='" + p_text + '\'' +
				", flag=" + flag +
				'}';
	}
}
