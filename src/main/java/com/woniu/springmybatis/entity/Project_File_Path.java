package com.woniu.springmybatis.entity;

import javax.persistence.Table;

/**
 * @author YuJunHong
 */
@Table(name = "project_file_path")
public class Project_File_Path {
    private Integer id;
    private String project_file_path;
    private Integer pro_id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProject_file_path() {
        return project_file_path;
    }

    public void setProject_file_path(String project_file_path) {
        this.project_file_path = project_file_path;
    }

    public Integer getPro_id() {
        return pro_id;
    }

    public void setPro_id(Integer pro_id) {
        this.pro_id = pro_id;
    }



}
