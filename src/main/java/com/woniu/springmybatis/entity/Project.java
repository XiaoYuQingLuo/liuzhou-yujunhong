package com.woniu.springmybatis.entity;

public class Project {
    private Integer id;
    private String pro_name;
    private Integer user_id;
    private Integer inspector_id;
    private Integer business_id;
    private Integer pro_state;
    private String start_time;
    private String finish_time;
    private String pro_level;
    private String type;
    private String pro_text;
    private String funds;
    private String ground;
    private String build_time;
    private String update_time;
    private String flag;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPro_name() {
        return pro_name;
    }

    public void setPro_name(String pro_name) {
        this.pro_name = pro_name;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getInspector_id() {
        return inspector_id;
    }

    public void setInspector_id(Integer inspector_id) {
        this.inspector_id = inspector_id;
    }

    public Integer getBusiness_id() {
        return business_id;
    }

    public void setBusiness_id(Integer business_id) {
        this.business_id = business_id;
    }

    public Integer getPro_state() {
        return pro_state;
    }

    public void setPro_state(Integer pro_state) {
        this.pro_state = pro_state;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getFinish_time() {
        return finish_time;
    }

    public void setFinish_time(String finish_time) {
        this.finish_time = finish_time;
    }

    public String getPro_level() {
        return pro_level;
    }

    public void setPro_level(String pro_level) {
        this.pro_level = pro_level;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPro_text() {
        return pro_text;
    }

    public void setPro_text(String pro_text) {
        this.pro_text = pro_text;
    }

    public String getFunds() {
        return funds;
    }

    public void setFunds(String funds) {
        this.funds = funds;
    }

    public String getGround() {
        return ground;
    }

    public void setGround(String ground) {
        this.ground = ground;
    }

    public String getBuild_time() {
        return build_time;
    }

    public void setBuild_time(String build_time) {
        this.build_time = build_time;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
