package com.woniu.springmybatis.entity;

public class Vi_notif_user_files {
    private Integer id;
    private String notif_text;
    private String notif_title;
    private Integer notif_state;
    private Integer recieve_user_id;
    private Integer send_user_id;
    private String send_user;
    private String send_time;
    private Integer flag;
    private String name;

    public Integer getRecieve_user_id() {
        return recieve_user_id;
    }

    public void setRecieve_user_id(Integer recieve_user_id) {
        this.recieve_user_id = recieve_user_id;
    }

    public Integer getSend_user_id() {
        return send_user_id;
    }

    public void setSend_user_id(Integer send_user_id) {
        this.send_user_id = send_user_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNotif_text() {
        return notif_text;
    }

    public void setNotif_text(String notif_text) {
        this.notif_text = notif_text;
    }

    public String getNotif_title() {
        return notif_title;
    }

    public void setNotif_title(String notif_title) {
        this.notif_title = notif_title;
    }

    public Integer getNotif_state() {
        return notif_state;
    }

    public void setNotif_state(Integer notif_state) {
        this.notif_state = notif_state;
    }

    public String getSend_user() {
        return send_user;
    }

    public void setSend_user(String send_user) {
        this.send_user = send_user;
    }

    public String getSend_time() {
        return send_time;
    }

    public void setSend_time(String send_time) {
        this.send_time = send_time;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
