package com.woniu.springmybatis.entity;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 个人的详细信息类
 * 包含
 * 账户信息
 * 用户信息
 * 所属部门
 * 所属职位
 * 所属局
 */
@Table(name = "vi_account_user_depatment_position_council")
public class Vi_account_user_depatment_position_council {
	@Id
	private Integer a_id;
	private String username;
	private String password;
	private Integer u_id;
	private String name;
	private String tel;
	private String email;
	private String sex;
	private Integer d_id;
	private String d_name;
	private Integer p_id;
	private String p_name;
	private Integer c_id;
	private String council_name;
	private Integer flag;

	public Integer getA_id() {
		return a_id;
	}

	public void setA_id(Integer a_id) {
		this.a_id = a_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getU_id() {
		return u_id;
	}

	public void setU_id(Integer u_id) {
		this.u_id = u_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Integer getD_id() {
		return d_id;
	}

	public void setD_id(Integer d_id) {
		this.d_id = d_id;
	}

	public String getD_name() {
		return d_name;
	}

	public void setD_name(String d_name) {
		this.d_name = d_name;
	}

	public Integer getP_id() {
		return p_id;
	}

	public void setP_id(Integer p_id) {
		this.p_id = p_id;
	}

	public String getP_name() {
		return p_name;
	}

	public void setP_name(String p_name) {
		this.p_name = p_name;
	}

	public Integer getC_id() {
		return c_id;
	}

	public void setC_id(Integer c_id) {
		this.c_id = c_id;
	}

	public String getCouncil_name() {
		return council_name;
	}

	public void setCouncil_name(String council_name) {
		this.council_name = council_name;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	@Override
	public String toString() {
		return "Vi_account_user_depatment_position_council{" +
				"a_id=" + a_id +
				", username='" + username + '\'' +
				", password='" + password + '\'' +
				", u_id=" + u_id +
				", name='" + name + '\'' +
				", tel='" + tel + '\'' +
				", email='" + email + '\'' +
				", sex='" + sex + '\'' +
				", d_id=" + d_id +
				", d_name='" + d_name + '\'' +
				", p_id=" + p_id +
				", p_name='" + p_name + '\'' +
				", c_id=" + c_id +
				", council_name='" + council_name + '\'' +
				", flag=" + flag +
				'}';
	}
}
