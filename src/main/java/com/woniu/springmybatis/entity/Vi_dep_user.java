package com.woniu.springmybatis.entity;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 对应视图vi_dep_user，展示部门详情和部门下属员工
 */
@Table(name = "vi_dep_user")
public class Vi_dep_user {
	// 部门id
	private Integer id;
	// 部门简介
	private String d_text;
	// 部门下属员工
	private String user_name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getD_text() {
		return d_text;
	}

	public void setD_text(String d_text) {
		this.d_text = d_text;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	@Override
	public String toString() {
		return "Vi_dep_user{" +
				"id=" + id +
				", d_text='" + d_text + '\'' +
				", user_name='" + user_name + '\'' +
				'}';
	}
}
