package com.woniu.springmybatis.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 用户的操作记录实体
 */
@Table(name = "user_operatelog")
public class User_operatelog {
    @Id
    private int id;//主键
    @Column(name = "user_id")
    private Integer user_id;//用户id
    private String module_name;/**操作方法*/
    private String operate_desc;/**操作描述*/
    private String ip_address;//ip地址
    private Date time;//操作时间

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getModule_name() {
        return module_name;
    }

    public void setModule_name(String module_name) {
        this.module_name = module_name;
    }

    public String getOperate_desc() {
        return operate_desc;
    }

    public void setOperate_desc(String operate_desc) {
        this.operate_desc = operate_desc;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
