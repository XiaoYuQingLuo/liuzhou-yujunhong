package com.woniu.springmybatis.entity;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 部门表对应实体类
 */
@Table(name = "department")
public class Department {
    @Id
    private Integer id;
    private String d_name;
    private String d_text;
    private Integer council_id;
    private Integer flag;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getD_name() {
        return d_name;
    }

    public void setD_name(String d_name) {
        this.d_name = d_name;
    }

    public String getD_text() {
        return d_text;
    }

    public void setD_text(String d_text) {
        this.d_text = d_text;
    }

    public Integer getCouncil_id() {
        return council_id;
    }

    public void setCouncil_id(Integer council_id) {
        this.council_id = council_id;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", d_name='" + d_name + '\'' +
                ", d_text='" + d_text + '\'' +
                ", council_id=" + council_id +
                ", flag=" + flag +
                '}';
    }
}
