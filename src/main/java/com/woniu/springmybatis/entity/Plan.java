package com.woniu.springmybatis.entity;

import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "plan")
public class Plan {
    @Id
    private int    id;
    // 用户id
    private int    plan_user_id;
    // 部门ID
    private int    plan_dep_id;
    // 计划等级
    private int    plan_level;
    // 计划类型
    private int    plan_type;
    // 计划标题
    private String plan_title;
    // 计划状态
    private int    plan_state_id;
    // 计划审批人
    private int    plan_auditor_id;
    // 计划时间
    private Date   plan_time;
    // 计划内容
    private String plan_text;
    private int    flag;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPlan_user_id() {
        return plan_user_id;
    }

    public void setPlan_user_id(int plan_user_id) {
        this.plan_user_id = plan_user_id;
    }

    public int getPlan_dep_id() {
        return plan_dep_id;
    }

    public void setPlan_dep_id(int plan_dep_id) {
        this.plan_dep_id = plan_dep_id;
    }

    public int getPlan_level() {
        return plan_level;
    }

    public void setPlan_level(int plan_level) {
        this.plan_level = plan_level;
    }

    public int getPlan_type() {
        return plan_type;
    }

    public void setPlan_type(int plan_type) {
        this.plan_type = plan_type;
    }

    public String getPlan_title() {
        return plan_title;
    }

    public void setPlan_title(String plan_title) {
        this.plan_title = plan_title;
    }

    public int getPlan_state_id() {
        return plan_state_id;
    }

    public void setPlan_state_id(int plan_state_id) {
        this.plan_state_id = plan_state_id;
    }

    public int getPlan_auditor_id() {
        return plan_auditor_id;
    }

    public void setPlan_auditor_id(int plan_auditor_id) {
        this.plan_auditor_id = plan_auditor_id;
    }

    public Date getPlan_time() {
        return plan_time;
    }

    public void setPlan_time(Date plan_time) {
        this.plan_time = plan_time;
    }

    public String getPlan_text() {
        return plan_text;
    }

    public void setPlan_text(String plan_text) {
        this.plan_text = plan_text;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }


}
